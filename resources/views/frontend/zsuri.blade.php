@extends('frontend.layouts.default')
@section('title')
    A zsűri - Stilus és Otthon
@stop
@section('content')
<div class="container">

        @foreach($zsuri as $item)
        <a href="/zsuri/{{$item->slug}}" title="{{$item->name}}" class="zsuri-item">
            <div data-mh="zsuri" class="col-xs-4 col-md-2 col-md-push-5 image">

                    <img class="img-responsive center-block" src="{{'images/zsuri/'.$item->image}}" alt="{{$item->name}}"/>

            </div>

            <div data-mh="zsuri" class="col-xs-8  col-md-4 col-md-push-5 more">
                <div class="name">{{$item->name}}</div>
                <div class="title">{{$item->title}}</div>

            </div>
        </a>
        @endforeach


</div>
@stop()