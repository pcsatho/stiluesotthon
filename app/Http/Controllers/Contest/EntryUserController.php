<?php

namespace App\Http\Controllers\Contest;

use App\contestUser;
use App\Http\Controllers\Controller;
use App\Http\Requests\storeContestUserRequest;
use App\User;
use Intervention\Image\Facades\Image;

class EntryUserController extends Controller {

	public function postProfile( storeContestUserRequest $request ) {
		/*Grab auth user*/
		$user = \Auth::user();

		$userDir = $this->getUserDir( $user );

		$user->email = $request->get( 'email' );

		$contestUser = $user->contestant;

		$contestUser->website        = $request->get( 'website' );
		$contestUser->name           = $request->get( 'name' );
		$user->name           		 = $request->get( 'name' );
		$contestUser->info           = $request->get( 'info' );
		$contestUser->phone          = $request->get( 'phone' );
		$contestUser->postal_address = $request->get( 'postal_address' );
		$contestUser->school         = $request->get( 'school' );

		if ( $name = $this->moveUploadedFile( $user, $request, 'school_document' ) ) {
			$contestUser->school_document = $name;

		}

		if ( $name = $this->moveUploadedFile( $user, $request, 'agreement' ) ) {
			$contestUser->agreement = $name;

		}

		if ( $name = $this->moveUploadedImage( $user, $request, 'profile_pic' ) ) {

			$contestUser->profile_pic = $name;

		}

		if ( $request->has( '_small_profile_pic' ) ) {
			$fileName = 'small-' . $user->unique . '-profile_pic.jpg';
			$this->resizeCroppedImage( $request->get( '_small_profile_pic' ), 200, 200, $userDir, $fileName );
		}
		if ( $request->has( '_cropped_profile_pic' ) ) {
			$fileName = 'cropped-' . $user->unique . '-profile_pic.jpg';
			$this->resizeCroppedImage( $request->get( '_cropped_profile_pic' ), 460, 280, $userDir, $fileName );
		}

		if ( $user->save() && $contestUser->save()) {

			return \Redirect::back()->withMessage( 'A Profil frissítése sikeres!' );


		}
		else {
			return \Redirect::back()->withMessage( 'A Profil frissítése sikertelen!' );
		}




	}

	/**
	 * @param                         $user
	 * @param storeContestUserRequest $request
	 * @param                         $userDir
	 *
	 * @return mixed
	 */
	private function moveUploadedFile( $user, storeContestUserRequest $request, $field ) {

		if ( $request->hasFile( $field ) ) {

			$userDir  = public_path( 'users/' . $user->unique . '/' );
			$fileName = $user->unique . '-' . $field . '.' . $request->file( $field )->guessClientExtension();

			if ( $request->file( $field )->move( $userDir, $fileName ) ) {
				return $user->unique . '-' . $field . '.' . $request->file( $field )->guessClientExtension();
			}
		}

		return false;

	}

	private function moveUploadedImage( $user, storeContestUserRequest $request, $field ) {
		if ( $request->hasFile( $field ) ) {
			$userDir  = public_path( 'users/' . $user->unique . '/' );
			$fileName = $user->unique . '-' . $field . '.jpg';

			if ( $request->file( $field )->move( $userDir, $fileName ) ) {

				$smallImage   = Image::make( $userDir . $fileName );
				$croppedImage = Image::make( $userDir . $fileName );

				$smallImage->resize( 200, null, function ( $constraint ) {
					$constraint->aspectRatio();
				} )->crop( 200, 200 )->encode( 'jpg', 75 )->save( $userDir . 'small-' . $fileName );
				$croppedImage->resize( 460, null, function ( $constraint ) {
					$constraint->aspectRatio();
				} )->crop( 460, 280 )->encode( 'jpg', 75 )->save( $userDir . 'cropped-' . $fileName );

				return $fileName;
			}
			\Log::error('Profilkép feltöltés hiba',['user'=>$user,'request'=>$request,'field'=>$field]);

		}

		return false;
	}

	function resizeCroppedImage( $file, $width, $height, $saveDir, $fileName ) {

		if ( ! empty( $file ) ) {
			$destinationPath = $saveDir;

			$file = str_replace( 'data:image/png;base64,', '', $file );
			$img  = str_replace( ' ', '+', $file );
			$data = base64_decode( $img );

			$image = Image::make( $data );
			$image = $image->resize( $width, $height )->encode( 'jpg', 75 )->save( $destinationPath . $fileName );

			if ( $image ) {
				return $destinationPath . $fileName;
			}
		}
	}

	/**
	 * @param $user
	 *
	 * @return string
	 */
	private function getUserDir( $user ) {
		if ( ! \File::isDirectory( public_path( 'users' ) ) ) {
			\File::makeDirectory( public_path( 'users' ) );
		}

		$userDir = public_path( 'users/' . $user->unique . '/' );

		if ( ! \File::isDirectory( $userDir ) ) {
			\File::makeDirectory( $userDir );

			return $userDir;
		}

		return $userDir;
	}

}