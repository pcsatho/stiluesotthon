<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEgyediButorToContestEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contest_entry', function (Blueprint $table) {
            $table->string('egyedi_butor',50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest_entry', function (Blueprint $table) {
            $table->dropColumn('egyedi_butor');
        });
    }
}
