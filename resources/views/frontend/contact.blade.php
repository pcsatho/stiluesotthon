@extends('frontend.layouts.default')
@section('title')
Kapcsolat - Stilus és Otthon
@stop
@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12 text-center">
            <h1>
                Style&Home Kft.
            </h1>
                <p><a href="tel:+36-30-349-7161"><i class="fa fa-phone fa-2x">+3630 349 7161</i></a></p>
                <p><a href="mailto:lakberendezo@styleandhome.hu"><i class="fa fa-envelope fa-2x"> lakberendezo@styleandhome.hu</i></a></p>
        </div>
    </div>

    <div id="contact-persons" class="row text-center">

        <div class="col-xs-12 col-sm-4 col-md-4">
            <img src="{{asset('/images/nemess.viktor.jpg')}}" alt="Nemess Viktor" class="img-responsive center-block"/>
            <h3>Nemess Viktor</h3>
            <h4>ügyvezető</h4>
            <a class="mail" href="mailto:nemess.viktor@styleandhome.hu">
                nemess.viktor@styleandhome.hu
            </a>
            <a class="tel" href="tel:+36707707053">
                +3670 770 7053
            </a>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <img src="{{asset('/images/Torok_Bernadett_1.JPG')}}" alt="Török Bernadett" class="img-responsive center-block"/>

            <h3>Török Bernadett</h3>
            <h4>szakmai referens </h4>
            <a class="mail" href="mailto:lakberendezo@styleandhome.hu">
                lakberendezo@styleandhome.hu
            </a>
            <a class="tel" href="tel:+36303497161">
                +3630 349 7161
            </a>
        </div>
        <div class="col-xs-12 col-sm-4 col-md-4">
            <img src="{{asset('/images/szabo_dora.jpg')}}" alt="Szabó Dóra"
                 class="img-responsive center-block"/>

            <h3>Szabó Dóra</h3>
            <h4>marketing vezető</h4>
            <a class="mail" href="mailto:szabo.dora@styleandhome.hu">
                szabo.dora@styleandhome.hu
            </a>
            <a class="tel" href="tel:+36304657023">
                +3630 465 7023
            </a>
        </div>

    </div>

        </div>

@stop()