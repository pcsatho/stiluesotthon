<?php

namespace App\Http\Controllers\Contest;

use App\Http\Controllers\Controller;


class BackendController extends Controller{

	public function __construct()
	{

		$user = \Auth::user();
		$user->load('contestant');

		if ( is_null($user->contestant) )
		{

			$contest_user = new \App\contestUser();
			$contest_user->name = $user->name;


			$user = \Auth::user();
			$user->contestant()->save($contest_user);


		}

	}

	public function getPanel()
	{
		$completion=array();
		if( \Auth::user()->contestant && \Auth::user()->contestant->entries){
		foreach(\Auth::user()->contestant->entries as $entry){
				$resp = \App\contestEntry::checkCompletion($entry);
				array_push($completion,$resp);
		}
		}
		$profile_completion = \App\contestUser::checkData(\Auth::user());

		return view('backend.index')->withCompletion($completion)->withProfileCompletion($profile_completion);
	}


	public function getHelp()
	{
		return view('backend.help');
	}



	public function getProfile()
	{
		$user = \Auth::user();
		$user->load('contestant');


		return view('backend.profile')->with('user',$user);
	}



}