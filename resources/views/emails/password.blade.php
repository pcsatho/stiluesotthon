@extends('emails.layout')
@section('title')
Elfelejtett jelszó
@stop
@section('content')
    <h1>Kedves felhasználónk!</h1>
    Ön jelszóemlékeztetőt kért a <a href="{{url('/')}}"> Stílus és Otthon </a> weboldalán.
    Kattintson ide jelszava megváltoztatásához: <a href="{{ url('elfelejtett-jelszo/reset/'.$token) }}">{{
        url('elfelejtett-jelszo/reset/'.$token) }}</a>


    Amennyiben a jelszó emlékeztetést nem Ön kezdeményezte, kérjük hagyja figyelmen kívül ezt az e-mailt.

@stop
