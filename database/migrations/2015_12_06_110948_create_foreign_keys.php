<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Eloquent\Model;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('contest_user', function(Blueprint $table) {
			$table->foreign('user_id')->references('id')->on('users')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('contest_entry', function(Blueprint $table) {
			$table->foreign('contest_user_id')->references('id')->on('contest_user')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
		Schema::table('contest_image', function(Blueprint $table) {
			$table->foreign('contest_entry_id')->references('id')->on('contest_entry')
						->onDelete('restrict')
						->onUpdate('restrict');
		});
	}

	public function down()
	{
		Schema::table('contest_user', function(Blueprint $table) {
			$table->dropForeign('contest_user_user_id_foreign');
		});
		Schema::table('contest_entry', function(Blueprint $table) {
			$table->dropForeign('contest_entry_contest_user_id_foreign');
		});
		Schema::table('contest_image', function(Blueprint $table) {
			$table->dropForeign('contest_image_contest_entry_id_foreign');
		});
	}
}