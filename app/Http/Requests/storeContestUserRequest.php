<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class storeContestUserRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {




        if(!\Auth::user()){

            return false;
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'website' => 'url|max:50',
            'name' => 'required|max:50',
            'info'=>'required|max:500',
            'phone' => 'required|max:15',
            'postal_address' => 'required|max:70',
            'school' => 'required',
            'school_document' => 'required_without:_school_document|mimes:pdf,PDF ',
            'agreement' => 'required_without:_agreement|mimes:pdf,PDF ',
            'profile_pic' => 'required_without:_profile_pic|image',

        ];
    }
}
