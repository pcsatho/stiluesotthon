<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class UserController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        /** Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\Category);
			$this->filter->add('name', 'Name', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('name', 'Name');
			$this->grid->add('code', 'Code');
			$this->addStylesToGrid();

        */
		$this->filter = \DataFilter::source(new \App\User);
		$this->filter->add('name', 'Név', 'text');
		$this->filter->add('email', 'email', 'text');
		$this->filter->submit('search');
		$this->filter->reset('reset');
		$this->filter->build();

		$this->grid = \DataGrid::source($this->filter);
		$this->grid->add('name', 'Név');
		$this->grid->add('email', 'E-mail');
		$this->grid->add('unique', 'Egyedi azonosító');

		$this->addStylesToGrid();

		return $this->returnView();

    }
    
    public function  edit($entity){
        
        parent::edit($entity);

        /* Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\Category());

			$this->edit->label('Edit Category');

			$this->edit->add('name', 'Name', 'text');
		
			$this->edit->add('code', 'Code', 'text')->rule('required');


        */

		$this->edit = \DataEdit::source(new \App\User());

		$this->edit->label('Felhasználó Adatai');

		$this->edit->add('name', 'Név', 'text')->rule('required');
		$this->edit->add('password', 'Jelszó', 'password')->rule('required');
		$this->edit->add('email', 'E-mail cím', 'text')->rule('required')->rule('email');

		return $this->returnEditView();
    }    
}
