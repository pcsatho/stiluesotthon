@extends('backend.dashboard')

@section('content')



    
   <h2>Tervezési segédletek</h2>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekTmZuYjNuSVJwYmc&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/elkoep.jpg')}}" alt="Elko EP"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekWkYxTG5qdnZjWVE&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/inels.jpg')}}" alt="iNELS"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0BxivUaLZPs-qclliNmZaOWl0VjQ&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/forest.jpg')}}" alt="Forest"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekNW9CVjcyLTFWbUU&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/kotar.jpg')}}" alt="Kő-tár"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/egger.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/egger.jpg')}}" alt="IBD - EGGER"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/egger.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/haro.jpg')}}" alt="IBD - HARO"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/egger.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/aquastep.jpg')}}" alt="IBD - AQUA-STEP"/></a></div>

    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/marmorin.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/marmorin.jpg')}}" alt="IBD - MARMORIN"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekU09pYXhQS0N0TE0&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/kludi.jpg')}}" alt="Kludi"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/mapei.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/mapei.jpg')}}" alt="Mapei"/></a></div>

    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekRVRHM3RyNl9TX2c&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/nivo.jpg')}}" alt="Nivo Home"/></a></div>

    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/ravak.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/ravak.jpg')}}" alt="Ravak"/></a></div>

    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/toptrade.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/toptrade.jpg')}}" alt="Top Trade"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekSjl3YURiLXQ1OTg&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/trilak.jpg')}}" alt="Trilak"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekb2t3OWltQnlaQ2M&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/zalakeramia.jpg')}}" alt="Zalakerámia"/></a></div>
    <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekVG80dWRMZTB0eTQ&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/arrital.jpg')}}" alt="Arrital"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/SitIn_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/sitin.jpg')}}" alt="SitIn"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/Sicis_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/sicis.jpg')}}" alt="Sicis"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/IDEAL_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/ideal.jpg')}}" alt="Ideal"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/FORBO_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/forbo.jpg')}}" alt="Forbo"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/Flotex_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/flotex.jpg')}}" alt="Flotex"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/EGE_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/ege.jpg')}}" alt="Ege"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/Bordbar_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/bordbar.jpg')}}" alt="Bordbar"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/Amtico_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/amt.jpg')}}" alt="Amtico"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="{{asset('files/Vescom_tervezesi_segedlet.pdf')}}"><img class="img-responsive center-block" src="{{asset('images/partner/logo/vescom.jpg')}}" alt="Vescom"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="http://www.rovitex.hu/downloads/20151026nt/StyleAndHome/"><img class="img-responsive center-block" src="{{asset('images/partner/logo/rovitex.jpg')}}" alt="Rovitex"/></a></div>

   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="http://whirlpool.hu/tamogatas/katalogusok"><img class="img-responsive center-block" src="{{asset('images/partner/logo/whirlpool.jpg')}}" alt="Whirlpool"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekRUIzanBUMVdqdzQ&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/hofstadter.jpg')}}" alt="Hofstadter"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekQlRqUkxmTndLY0k&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/zehnder.jpg')}}" alt="Zehnder"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekdm5PakhnVkgzdk0&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/palatinus.jpg')}}" alt="Palatinus"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekb0VHdkRLVXRTZDA&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/otti.jpg')}}" alt="OTTI"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekUThwUklRcjAxWGc&usp=sharing "><img class="img-responsive center-block" src="{{asset('images/partner/logo/rigips.jpg')}}" alt="Rigips"/></a></div>
   <div class="col-xs-6 col-sm-3 col-md-2 mb-30"><a target="_blank" href="https://drive.google.com/folderview?id=0B13XSyvWcFekMG9uWV9zbXFHSzg&usp=sharing"><img class="img-responsive center-block" src="{{asset('images/partner/logo/budatech.jpg')}}" alt="Budatech"/></a></div>





@endsection