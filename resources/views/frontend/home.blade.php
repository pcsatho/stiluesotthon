@extends('frontend.layouts.default')
@section('title')
    Főoldal - Stilus és Otthon
@stop
@section('content')
<div class="container">
<div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-push-2">
           <div id="logo">
               <h1><i class="so-icon so-icon-logo red"></i></h1>
           </div>
            <div id="home-social" class="clearfix text-center">
                <ul class="nav navbar-nav">
                    <li>
                        <a target="_blank" data-trigger="hover focus" data-toggle="tooltip" data-placement="top" title="Keressen minket facebookon!" href="https://www.facebook.com/stilusesotthon/">
                            <i class="so-icon so-icon-f_icon "></i>
                        </a>
                    </li>
                    <li>
                        <a data-trigger="hover focus" data-toggle="tooltip" data-placement="top" title="Írjon nekünk a lakberendezo@styleandhome.hu e-mail címre" href="mailto:lakberendezo@styleandhome.hu">
                            <i class="so-icon so-icon-letter_icon"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div id="home-content" class="clearfix text-center">
            <h2 class="text-center text-uppercase">Pályázzon Ön is – legyen 2016 az Ön éve!
            </h2>
                <p>Szereti a kihívásokat? Szívesen megmérettetné magát, elismertséget és népszerűséget szerezve mindezzel – még
                    hozzá egy komoly szakmai zsűri előtt? Akkor jelentkezzen Ön is a <b>Stílus&Otthon</b> összefogás idén
                    először debütáló <b>Style&Draw</b> szakmai pályázatára és 2016-ban zsebelje be Ön a legtöbb ajánlást!
                </p>
                <p>A <b>Stílus&Otthon</b> együttműködés Magyarország piacvezető márkáinak összefogása – a kooperáció fő célja az
                    építkezők/felújítók segítése komplex megoldásokkal, előremutató – minőségi termékekkel és szolgáltatásokkal.</p>
                <p>  Ezzel párhuzamosan a <b>Style&Draw</b> pályázat kihirdetésével célunk a tervezői szakma népszerűsítése és
                    eljutatása az építkezők/felújítók körébe – annak érdekében, hogy leendő otthonuk tervezői közreműködéssel
                    válhasson egy igazán élvezhető és hosszútávon élhető otthonná.</p>

                <p>


                <div class="btn-group padded">
                    <a class="btn btn-white btn-inline" href="{{URL::to('palyazat')}}">Tovább a részletekért</a>
                    <a class="btn btn-white btn-inline" target="_blank" href="{{asset('files/palyazati_kiiras.pdf')}}">Pályázati kiírás</a>
                </div>


            </div>


                <a class="btn btn-register text-center text-uppercase" href="{{URL::to('/verseny/regisztracio')}}">Regisztráljon itt és most!</a>



        </div>
</div>
</div>
@stop()