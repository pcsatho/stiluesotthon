@extends('frontend.layouts.default')
@section('title')
    Elfelejtett jelszó - Stilus és Otthon
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-push-2 col-md-6 col-md-push-3">

                <form method="POST" action="{{URL::to('/elfelejtett-jelszo')}}">
                    {!! csrf_field() !!}
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach

                            </ul>
                        </div>
                    @endif


                    <div class="form-group">
                        <label for="email"> E-mail cím</label>
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">Jelszóemlékeztető küldése</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
