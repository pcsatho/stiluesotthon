<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contestUser extends Model
{
	protected $table = 'contest_user';
	protected $guarded = ['id'];

	static $schoolList = array(
			'bme'     => array(
					'name'  => 'Budapesti Műszaki Egyetem',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),
			'bkf'     => array(
					'name'  => 'BKF / Budapesti Metropolitan Egyetem',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),
			'de'     => array(
					'name'  => 'Debreceni Egyetem',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),
			'mome'     => array(
					'name'  => 'Moholy-Nagy Művészeti Egyetem',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),
			'nyme'     => array(
					'name'  => 'Nyugat-magyarországi Egyetem',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),
			'pte'     => array(
					'name'  => 'Pécsi Tudományegyetem',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),
			'szie-ybl'     => array(
					'name'  => 'Szent István Egyetem Ybl Miklós Építéstudományi Kar',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),
			'szie'     => array(
					'name'  => 'Széchenyi István Egyetem',
					'image' => 'egy.jpg',
					'category'=>'egyetem',
			),



			'junior-art'     => array(
					'name'  => 'Junior Art Lakberendező Iskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'stilushaz'     => array(
					'name'  => 'Stílusház Kreatív Művészeti Magániskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'krea'     => array(
					'name'  => 'Krea Kortárs Művészeti Iskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'szimultan'     => array(
					'name'  => 'Szimultán Művészeti Iskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'kurzus'     => array(
					'name'  => 'Kurzus Lakberendező Iskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'jovenker'     => array(
					'name'  => 'Jovenker Művészeti és Divatiskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'oktav'     => array(
					'name'  => 'OKTÁV',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'efeb'     => array(
					'name'  => 'EFEB',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'athene'     => array(
					'name'  => 'Athéné Szakközépiskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'szitakoto'     => array(
					'name'  => 'Szitakötő Kreatívsuli',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'kozma'     => array(
					'name'  => 'Kozma Lajos Faipari Szakközépiskola',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'werk'     => array(
					'name'  => 'Werk Akadémia',
					'image' => 'egy.jpg',
					'category'=>'iskola',
			),
			'losz'     => array(
					'name'  => 'Lakberendezők Országos Szövetsége',
					'image' => 'egy.jpg',
					'category'=>'egyeb',
			),


	);


	public static function getSchoolList()
	{
	$list = array();
		$list[''] = '';
		foreach (self::$schoolList as $key => $value)
		{
			$list[$key]= $value['name'];

		}

		return $list ;

	}


	public static function getProfilePic(User $user)
	{
			$fileName = public_path().'/users/' . $user->unique . '/small-' . $user->contestant->profile_pic;

		return \URL::to('users/' . $user->unique . '/small-' . $user->contestant->profile_pic . '?'.filemtime($fileName));

	}


	public static function getFullPic(User $user)
	{
		$fileName = public_path().'/users/' . $user->unique . '/' . $user->contestant->profile_pic;
		return \URL::to('users/' . $user->unique . '/' . $user->contestant->profile_pic.'?'.filemtime($fileName));

	}

	public static function getCroppedPic(User $user)
	{
		$fileName = public_path().'/users/' . $user->unique . '/cropped-' . $user->contestant->profile_pic;
		return \URL::to('users/' . $user->unique . '/cropped-' . $user->contestant->profile_pic.'?'.filemtime($fileName));

	}


	public static function getAgreement(User $user)
	{

		return \URL::to('users/' . $user->unique . '/' . $user->contestant->agreement);

	}


	public static function getSchoolDocument(User $user)
	{

		return \URL::to('users/' . $user->unique . '/'. $user->contestant->school_document);

	}

	public function entries()
	{
		return $this->hasMany('App\contestEntry');
    }


	public function user()
	{
		return $this->belongsTo('App\User','user_id');
	}


	public static function checkData(User $user)
	{
		$html   = "";
		$status = true;
		$html .= "<table class='table table-striped'>";


		if ( ! empty( $user->name ) )
		{
			$html .= "<tr><td>Név </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Név </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}

		if ( ! empty( $user->contestant->info) )
		{
			$html .= "<tr><td>Rövid bemutatkozás </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Rövid bemutatkozás  </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}
		if ( ! empty( $user->contestant->postal_address ) )
		{
			$html .= "<tr><td>Postai cím </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Postai cím  </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}
		if ( ! empty( $user->contestant->website ) )
		{
			$html .= "<tr><td>Weboldal (nem kötelező) </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Weboldal (nem kötelező)  </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}

		if ( ! empty( $user->contestant->phone ) )
		{
			$html .= "<tr><td>Telefonszám </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Telefonszám </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}

		if ( ! empty( $user->contestant->school ) )
		{
			$html .= "<tr><td>Bizonyítványt kibocsájtó intézmény </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Bizonyítványt kibocsájtó intézmény  </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}

		if ( ! empty( $user->contestant->school_document ) )
		{
			$html .= "<tr><td>Végzettséget igazoló irat </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Végzettséget igazoló irat  </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}

		if ( ! empty( $user->contestant->agreement ) )
		{
			$html .= "<tr><td>Nyilatkozat </td><td><i class='fa fa-check'></i></td></tr>";
		}
		else
		{
			$html .= "<tr><td>Nyilatkozat </td><td><i class='fa fa-times'></i></td></tr>";
			$status = false;
		}
		$html .= "</table>";

		$response = array(
			'html'   => $html,
			'status' => $status
		);

		return $response;
	}
}
