<?php 

namespace App;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Database\Eloquent\Model;

class Partner extends Model implements SluggableInterface {

    use SluggableTrait;
    protected $table = 'partner';


    protected $sluggable = [
        'build_from' => 'name',
        'save_to'    => 'slug',
    ];


	public function brands() {
		return $this->hasMany('App\Brand');
	}
}
