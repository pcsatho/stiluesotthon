<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToContestEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contest_entry', function (Blueprint $table)
        {
        $table->string('name',30);
        $table->string('info',150);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest_entry', function (Blueprint $table)
        {
        $table->dropColumn(array('name','info'));
        });
    }


}
