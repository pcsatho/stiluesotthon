<?php

namespace App\Http\Requests;

use App\contestEntry;
use App\Http\Requests\Request;

class contestEntryRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        if (!\Auth::check()) {
            return false;
        }

        if ($this->id) {
            $user = \Auth::user()->contestant;
            $entry = contestEntry::findOrFail($this->id);  // $id is a route parameter

            return $entry->contest_user_id === $user->id;
        } else {
            return true;
        }

    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return [
            'name' => 'max:30 | required',
            'style' => 'required',
            'entry_category' => 'required',
            'info' => 'required | max:150',
            'eredeti_alaprajz' => 'mimes:pdf,PDF|max:3000',
            'bontasi_rajz' => 'mimes:pdf,PDF|max:3000',
            'epitesi_rajz' => 'mimes:pdf,PDF|max:3000',
            'berendezesi_alaprajz' => 'mimes:pdf,PDF|max:3000',
            'falnezet_konyha' => 'mimes:pdf,PDF|max:3000',
            'falburkolasi_terv' => 'mimes:pdf,PDF|max:3000',
            'padloburkolat_kiosztas' => 'mimes:pdf,PDF|max:3000',
            'kapcsolasi_rajz' => 'mimes:pdf,PDF|max:3000',
            'muleiras' => 'mimes:pdf,PDF|max:3000',
            'konszignacios_tabla' => 'mimes:pdf,PDF|max:3000',
            'egyedi_butor' => 'mimes:pdf,PDF|max:3000',
            'almennyezet_terve' => 'mimes:pdf,PDF|max:3000'
        ];
	}


    public function forbiddenResponse()
    {
        return redirect()->back()->withInput()->withMessage('A kívánt művelet nem hajtható végre');
    }


    public function messages()
    {
        return [
            'attributes' => [
                'eredeti_alaprajz' => 'Eredeti Alaprajz',
                'bontasi_rajz' => 'Bontási rajz',
                'berendezesi_alaprajz' => 'Berendezési alaprajz',
                'falnezet_konyha' => 'Falnézet - konyha',
                'falburkolasi_terv' => 'Falburkolási Terv',
                'padloburkolat_kiosztas' => 'Padlóburkolat kiosztás',
                'kapcsolasi_rajz' => 'Kapcsolási rajz',
                'muleiras' => 'Műleírás',
                'konszignacios_tabla' => 'Konszignációs tábla',

            ],
        ];
    }
}
