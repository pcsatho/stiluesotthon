@extends('emails.layout')
@section('title')
Regisztráció a Stílus és Otthon oldalán
@stop
@section('content')

<h1>Kedves {{$user->name}}! </h1>

<p>Örömünkre szolgál, hogy regisztrált a Stílus&Otthon összefogás által kiírt Style&Draw szakmai pályázatra. </p>
<p>A belépéshez szüksége lesz az Ön által megadott jelszóra illetve az alábbi, regisztrációs rendszer által generált
    anonim felhasználónévre!</p>
<p><b>Felhasználónév: </b><i>{{$user->unique}} </i></p>
<p>A felhasználónév bejelentkezés után nem változtatható meg, így javasolt megőrizni ezt a levelet!<br/>
Jó munkát és nagyon sok sikert kívánunk a pályázathoz!</p>
<br/>
<p>
    <a href="{{URL::to('verseny/bejelentkezes')}}"> Bejelentkezéshez kattints ide!</a>
</p>



    @stop









