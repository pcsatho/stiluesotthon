@extends('frontend.layouts.default')
@section('title')
   Bejelentkezés - Stilus és Otthon
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-push-2 col-md-6 col-md-push-3">

                <form method="POST" action="{{URL::to('/verseny/bejelentkezes')}}">
                    {!! csrf_field() !!}

                    <div class="form-group">
                        <label for="unique"> Felhasználói azonosító</label>
                        <input type="text" name="unique" class="form-control" value="{{ old('unique') }}">
                    </div>

                    <div class="form-group">
                        <label for="password"> Jelszó</label>

                        <input type="password" name="password" class="form-control" id="password">
                    </div>

                    <div class="form-group">
                        <label for="remember"> Emlékezz Rám
                        <input type="checkbox" name="remember" id="remember" ></label>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">Bejelentkezés</button>
                        <a href="{{url('elfelejtett-jelszo')}}">Elfelejtett jelszó</a>
                    </div>
                </form>
            </div>


        </div>
    </div>
@stop