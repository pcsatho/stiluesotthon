<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cache;

class contestEntry extends Model
{

    protected $table = 'contest_entry';
    protected $guarded = ['id'];

    protected $appends = ['vote_count','position'];



    public $entries;


    static $styles = array(
        null => 'Válasszon a listából!',
        'minimal' => 'Minimál',
        'glamour' => 'Glamour',
        'loft' => 'Loft',
        'indusztrialis' => 'Indusztriális',
        'skandinav' => 'Skandináv',
        'retro' => 'Retro',
        'eklektikus' => 'Eklektikus',
        'klasszikus' => 'Klasszikus',
        'angol-szasz' => 'Angol szász',
        'vintage' => 'Vintage',
        'provence' => 'Provence',
        'mediterran' => 'Mediterrán',
        'country' => 'Country',
        'etno' => 'Etno',
        'art-deco' => 'Art deco',
        'modern-kortars' => 'Modern-Kortárs',
        'urbanista' => 'Urbanista'

    );

    static $fileFields = array(
        'eredeti_alaprajz' => 'Eredeti Alaprajz',
        'bontasi_rajz' => 'Bontási rajz',
        'epitesi_rajz' => 'Építési rajz',
        'berendezesi_alaprajz' => 'Berendezési alaprajz',
        'falnezet_konyha' => 'Falnézet - konyha',
        'falburkolasi_terv' => 'Falburkolási Terv',
        'padloburkolat_kiosztas' => 'Padlóburkolat kiosztás',
        'kapcsolasi_rajz' => 'Kapcsolási rajz',
        'egyedi_butor' => 'Egyedi bútor falnézeti rajza',
        'muleiras' => 'Műleírás',
        'konszignacios_tabla' => 'Konszignációs tábla',
        'almennyezet_terve' => 'Álmennyezet terve'
    );

    static $rooms = array(
        "nappali" => "Nappali",
        "haloszoba" => "Hálószoba",
        "konyha" => "Konyha",
        "gyerekszoba" => "Gyerekszoba",
        "furdoszoba" => "Fürdőszoba - WC",
        "otthoni-iroda" => "Otthoni iroda",
        "moodboard" => "Moodboard",
        "gardrobszoba" => "Gardróbszoba",
        "haztartasi-helyiseg" => "Háztartási helyiség",
        'Eloszoba-kozlekedo' => 'Előszoba/Közlekedő'

    );

    static $entry_categories = array(
        'egyedulallo' => "Egyedülálló",
        'fiatal-hazaspar' => "Fiatal házaspár egy gyerekkel",
        'kozepkoru-hazaspar' => "Középkorú házaspár, két nagy gyerekkel",

    );





    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($entry) {

            $userDir = public_path() . '/entries/' . \Auth::user()->unique . '/' . $entry->id . '/';


            $err = 0;
            \DB::beginTransaction();
            foreach ($entry->images as $image) {
                $del = $image->delete();
                if (!$del) $err = 1;
            }

            if ($err == 0) {
                \DB::commit();
                \File::deleteDirectory($userDir);
            } else {
                \DB::rollback();
            }

        });
    }

    public function user()
    {
        return $this->belongsTo('App\contestUser', 'contest_user_id');
    }

    public function images()
    {

        return $this->hasMany('App\contestImage');
    }

    public static function getStyles()
    {

        return self::$styles;

    }

    public static function getRooms()
    {

        return self::$rooms;

    }

    public static function getUploadedFile($entry_id, $file)
    {


        $entry = contestEntry::find($entry_id);
        $path = 'entries/' . $entry->user->user->unique . '/' . $entry_id . '/' . $file;

        if(\File::exists(public_path().'/'.$path)){

        $path = 'entries/' . $entry->user->user->unique . '/' . $entry_id . '/' . $file.'?'.filemtime(public_path().'/'.$path);
        }
            return \URL::to($path);

    }

    public function getCover()
    {


        $path = 'entries/' . $this->user->user->unique . '/' . $this->id . '/cover.jpg';
        if(\File::exists(public_path().'/'.$path)){

            $path = 'entries/' . $this->user->user->unique . '/' . $this->id . '/cover.jpg?'.filemtime(public_path().'/'.$path);
        }
        return \URL::to($path);

    }


    public static function checkCompletion($entry)
    {
        $html = "";
        $status = true;
        $html .= "<table class='table table-striped'>";
        $html .= "<tr><td colspan='2'><p>Az alábbi lista arra szolgál, hogy leellenőrizze, mely fájlokat kell még feltöltenie. <br>
					<b>Az álmennyezeti terv nem minden esetben kötelező!</b></p></td></tr>";
        if (!empty($entry->name)) {
            $html .= "<tr><td>Pályamunka neve </td><td><i class='fa fa-check'></i></td></tr>";
        } else {
            $html .= "<tr><td>Pályamunka neve </td><td><i class='fa fa-times'></i></td></tr>";
            $status = false;
        }

        if (!empty($entry->info)) {
            $html .= "<tr><td>Pályamunka leírása </td><td><i class='fa fa-check'></i></td></tr>";
        } else {
            $html .= "<tr><td>Pályamunka leírása </td><td><i class='fa fa-times'></i></td></tr>";
            $status = false;
        }
        if (!empty($entry->style)) {
            $html .= "<tr><td>Pályamunka stílusa </td><td><i class='fa fa-check'></i></td></tr>";
        } else {
            $html .= "<tr><td>Pályamunka stílusa  </td><td><i class='fa fa-times'></i></td></tr>";
            $status = false;
        }

        if (!empty($entry->entry_category)) {
            $html .= "<tr><td>Családmodell </td><td><i class='fa fa-check'></i></td></tr>";
        } else {
            $html .= "<tr><td>Családmodell </td><td><i class='fa fa-times'></i></td></tr>";
            $status = false;
        }

        foreach (self::$fileFields as $field => $name) {
            if (!empty($entry->$field)) {
                $html .= "<tr><td>" . $name . " </td><td><i class='fa fa-check'></i></td></tr>";
            } else {
                $html .= "<tr><td>" . $name . " </td><td><i class='fa fa-times'></i></td></tr>";
                $status = false;
            }
        }

        foreach (self::$rooms as $field => $name) {
            $checkRoom = contestImage::checkByRoom($field, $entry);
            if ($checkRoom['status']) {
                if (!($field == 'gyerekszoba' && $entry->entry_category == 'egyedulallo')) {
                    $html .= "<tr><td>" . $name . "(" . $checkRoom['count'] . "/" . $checkRoom['max'] . ") </td><td><i class='fa fa-check'></i></td></tr>";
                }

            } else {
                if (!($field == 'gyerekszoba' && $entry->entry_category == 'egyedulallo')) {
                    $html .= "<tr><td>" . $name . "(" . $checkRoom['count'] . "/" . $checkRoom['max'] . ") </td><td><i class='fa fa-times'></i></td></tr>";
                    $status = false;
                }


            }
        }

        $html .= "</table>";

        $response = array(
            'name' => $entry->name,
            'count' => $entry->images()->where('category', '!=', 'moodboard')->count(),
            'html' => $html,
            'status' => $status
        );

        return $response;
    }


    public function votes()
    {
        return $this->belongsToMany('App\User', 'votes', 'entry_id', 'id');
    }

    public function getVoteCountAttribute()

    {
        return $this->votes->count();
    }


    public function getPositionAttribute()

    {
            $entries = app('entries');

        $position = array_search($this->id, $entries);


        return (int)$position + 1;
    }


    public function getDirectoryAttribute()

    {
        return \URL::to('/entries/'.$this->user->user->unique.'/'.$this->id.'/');

    }




    public function scopeCategory($query, $type)
    {
        return $query->where('style', $type);
    }


}
