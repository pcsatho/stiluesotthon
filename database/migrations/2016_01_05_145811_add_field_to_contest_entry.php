<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldToContestEntry extends Migration
{

    public function up() {
        Schema::table( 'contest_entry', function ( Blueprint $table ) {
            $table->string( 'almennyezet_terve', 50 );
        } );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table( 'contest_entry', function ( Blueprint $table ) {
            $table->dropColumn( array( 'almennyezet_terve' ) );
        } );
    }
}
