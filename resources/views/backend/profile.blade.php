@extends('backend.dashboard')

@section('content')

    <h2>
        Profiladatok szerkesztése
    </h2>





@include('backend._partials.message')

    {!! Form::open(array('class'=>'','files'=>true)) !!}


    <div class="col-xs-12">
        <div class="form-group">
            {!!  Form::label('unique','Egyedi azonosító')  !!}
            {!!  Form::text('unique',$user->unique,array('disabled'=>'disabled','class'=>'form-control disabled')) !!}
        </div>

    </div>

    <div class="col-xs-12 col-sm-4">
        <div class="form-group">
            {!!  Form::label('profile_pic','Profil kép*')  !!}
            <span class="help-block">
                Megengedett filetípus: jpg.
            </span>
            {!!  Form::file('profile_pic',array('class'=>'form-input')) !!}
            @if(!empty($user->contestant->profile_pic))
                {!!  Form::hidden('_profile_pic',$user->contestant->profile_pic) !!}
                {!!  Form::hidden('_small_profile_pic',null,array('id'=>'smallProfilePicBase')) !!}
                {!!  Form::hidden('_cropped_profile_pic',null,array('id'=>'croppedProfilePicBase')) !!}
                <div class="col-xs-12">
                    <h3>Profilkép előnézet</h3>
                </div>
                <div class="col-sm-6">
                    <img class="img-responsive center-block"
                         src="{!! \App\contestUser::getProfilePic($user) !!}" alt=""/>
                </div>
                <div class="col-sm-6">
                    <img class="img-responsive center-block"
                         src="{!! \App\contestUser::getCroppedPic($user) !!}" alt=""/>
                </div>
                <div class="col-xs-12">
                    <button type="button" class="btn btn-red mt-15 btn-lg btn-block" data-toggle="modal"
                            data-target="#crop">
                        Profilkép szerkesztése
                    </button>
                </div>

                <div id="crop" class="modal fade" tabindex="-1" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">Profilkép szerkesztése</h4>
                            </div>
                            <div class="modal-body" style="width:100%">
                                <div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#small" aria-controls="home"
                                                                                  role="tab" data-toggle="tab">Négyzetes
                                                profilkép</a>
                                        </li>
                                        <li role="presentation"><a href="#cropped" aria-controls="profile" role="tab"
                                                                   data-toggle="tab">Téglalap alakú profilkép</a></li>

                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">

                                        <div role="tabpanel" class="tab-pane active" id="small">
                                            <img id="smallProfilePic" class="img-responsive center-block"
                                                 src="{!! \App\contestUser::getFullPic($user) !!}" alt=""/>
                                        </div>

                                        <div role="tabpanel" class="tab-pane" id="cropped">
                                            <img id="croppedProfilePic" class="img-responsive center-block"
                                                 src="{!! \App\contestUser::getFullPic($user) !!}" alt=""/>
                                        </div>

                                    </div>

                                </div>
                            </div>
                            <div style="text-align:center !important" class="modal-footer text-center">
                                Az ablak bezárása után kérjük kattintson a Profil mentése gombra, hogy a szerkesztett profilkép mentésre kerüljön!
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div><!-- /.modal -->
            @endif
        </div>
    </div>
    <div class="col-xs-12 col-sm-8">


        <div class="form-group">
            {!!  Form::label('name','Az Ön neve*')  !!}
            {!!  Form::text('name',$user->name,array('required'=>'required', 'class'=>'form-control')) !!}

        </div>

        <div class="form-group">
            {!!  Form::label('email','Az Ön e-mail címe')  !!}
            {!!  Form::email('email',$user->email,array('class'=>'form-control','required'=>'required')) !!}
            <span class="help-block"> A helyes formátum : valami@valami.hu </span>
        </div>

        <div class="form-group">
            {!!  Form::label('website','Az Ön weboldala')  !!}
            {!!  Form::url('website',$user->contestant->website,array('class'=>'form-control')) !!}
            <span class="help-block"> A helyes formátum : http://valami.hu/ </span>
        </div>

        <div class="form-group">
            {!!  Form::label('phone','Az Ön telefonszáma*')  !!}
            {!!  Form::text('phone',$user->contestant->phone,array('required'=>'required','class'=>'form-control')) !!}
            <span class="help-block"> A helyes formátum : +36-20-123-4567 </span>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="form-group">
            {!!  Form::label('info','Rövid bemutatkozás*')  !!}
            {!!  Form::textarea('info',$user->contestant->info,array('required'=>'required','class'=>'form-control')) !!}
            <span class="help-block"> 4-5 mondatos bemutatkozás önmagáról, és a szakmai hitvallásáról</span>
        </div>

        <div class="form-group">
            {!!  Form::label('postal_address','Az Ön Postai címe*')  !!}
            {!!  Form::text('postal_address',$user->contestant->postal_address,array('required'=>'required','class'=>'form-control')) !!}
            <span class="help-block"> A helyes formátum : 1096, Budapest Üllői út 100/B </span>
        </div>


        <div class="form-group">
            {!!  Form::label('school','Bizonyítványt kibocsájtó intézmény*')  !!}
            {!!  Form::select('school',\App\contestUser::getSchoolList(),$user->contestant->school,array('required'=>'required','class'=>'form-control')) !!}
        </div>
        <div class="form-group">
            {!!  Form::label('school_document','Végzettséget igazoló dokumentum*')  !!}
            <span class="help-block">
                Megengedett filetípus: pdf
            </span>
            {!!  Form::file('school_document',array('class'=>'form-input')) !!}
            @if(!empty($user->contestant->school_document))
                <a href="{!! \App\contestUser::getSchoolDocument($user) !!}">Jelenleg feltöltött végzettséget igazoló
                    dokumentum</a>
                {!!  Form::hidden('_school_document',$user->contestant->school_document) !!}
            @endif
        </div>
        <div class="form-group">
            {!!  Form::label('agreement','Nyilatkozat*')  !!}
            <span class="help-block">
                Megengedett filetípus: pdf
            </span>
            {!!  Form::file('agreement',array('class'=>'form-input')) !!}
            @if(!empty($user->contestant->agreement))
                <a href="{!! \App\contestUser::getAgreement($user) !!}">Jelenleg feltöltött nyilatkozat</a>
                {!!  Form::hidden('_agreement',$user->contestant->agreement) !!}

            @endif
        </div>

        <div class="form-group">
            {!!  Form::submit('Profil mentése',array('class'=>'btn btn-red')); !!}
        </div>
    </div>










    {!!   Form::close() !!}

@endsection