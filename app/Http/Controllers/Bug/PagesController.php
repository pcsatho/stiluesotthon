<?php

namespace App\Http\Controllers\Bug;


use App\Http\Controllers\Controller;


class PagesController extends Controller
{

	public function getIndex()
	{

		return view('frontend.home');
	}


	public function getPalyazat()
	{

		return view('frontend.palyazat');
	}


	public function getJury($slug)
	{

		$zsuri = \App\zsuri::findBySlug($slug);
		if($zsuri){
		return view('frontend.zsuri-single')->withZsuri($zsuri);
		}else{
			return abort(404);
		}
		}


	public function getJuries()
	{
		$zsuri = \App\zsuri::orderBy('position','asc')->get();
		return view('frontend.zsuri')->withZsuri($zsuri);
	}


	public function getPartners()
	{
		$partners = \App\Partner::orderBy('name')->get();

		return view('frontend.partner')->withPartners($partners);
	}


	public function getThankyou()
	{
		return view('frontend.thankyou');
	}


	public function getPartner($slug)
	{
		$partner = \App\Partner::with('brands')->where('slug','=',$slug)->first();
		$partner_link = substr($partner->web,7,strlen($partner->web));
		$partner_link2 = substr($partner->second_url,7,strlen($partner->second_url));
		if ( $partner )
		{
			return view('frontend.partner-single')->withPartner($partner)->withPartnerLink($partner_link)->withPartnerSecondLink($partner_link2);
		}
		else
		{
			return abort(404);
		}
	}




	public function getContact()
	{
		return view('frontend.contact');

	}


	public function getFAQ()
	{
		$faq = \App\Faq::orderBy('created_at')->get();
		return view('frontend.faq')->withFaq($faq);

	}


	public function getASZF()
	{

	}

}
