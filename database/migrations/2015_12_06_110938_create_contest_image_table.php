<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContestImageTable extends Migration {

	public function up()
	{
		Schema::create('contest_image', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('contest_entry_id')->unsigned();
			$table->string('src', 100);
			$table->string('name', 50);
			$table->string('category', 30)->index();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contest_image');
	}
}