<?php
return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'failed' => 'A beírt felhasználónév és/vagy jelszó téves.',
    'throttle' => 'Túl sok bejelentkezési kísérlet, kérjük próbálja újra :seconds másodperc múlva.',
];
