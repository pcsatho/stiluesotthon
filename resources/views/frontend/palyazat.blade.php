@extends('frontend.layouts.default')
@section('title')
    Jelentkezz a Style&Draw versenyre!  - Stilus és Otthon
@stop
@section('content')

    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-sm-8 col-sm-push-2 ">
                <h1>Style&Draw – az Ön pályázata! </h1>

                <p>Szereti a kihívásokat? Szívesen megmérettetné magát, elismertséget és népszerűséget szerezve
                    mindezzel – mindezt egy komoly szakmai zsűri előtt?
                    Ha igen, akkor pályázzon a <b>Style&Draw szakmai pályázatra!</b>
                    <br/>
                    <br/>
                    <b><a href="{{URL::to('/verseny/regisztracio')}}"> Engem érdekel, pályázom! </a></b></p>

                <p>Ön mit szólna egy olyan megrendelőhöz, aki már úgy keresi meg Önt, hogy tudja, hogy miben várhatja az
                    Ön segítségét, nem kell küzdenie, hanem könnyedén, kötekedés nélkül elfogadja az Ön megoldásait,
                    javaslatait?
                    Mi ebben szeretnénk segíteni a Style&Draw szakmai pályázattal! Eljuttatjuk az
                    építkezőkhöz/felújítókhoz Önt és a tervezői szakma értékeit, <a
                            href="{{URL::to('/verseny/regisztracio')}}">jelentkezzen a pályázatra</a> és járuljon
                    hozzá Ön is szakmája népszerűsítéséhez!
                </p>

                <h2>Mi ez a pályázat?</h2>

                <p>Biztosan Ön is tapasztalja a munkája során azt, hogy nem túl ismert és elfogadott az Ön szakmája az
                    építkezők/lakásfelújítók körében, mennyire nem tudják, hogy mi az Ön feladata, és miben tudná őket
                    segíteni a kivitelezés során.

                <p>

                </p>
                    A Stílus&Otthon együttműködés – ami Magyarország piacvezető építőipari márkáinak összefogása –
                    célja, hogy a lakberendező/design szakmát népszerűsítsük és közelebb juttassuk az
                    építkezőkhöz/felújítókhoz, hogy ez által Önnek sokkal könnyebbek legyenek a munkával töltött
                    mindennapjai.
                </p>

                <b> <a class="btn btn-primary pull-right" target="_blank" href="{{asset('files/palyazati_kiiras.pdf')}}">Elolvasom a pályázati kiírást!</a></b>
                <br>

                <h2> Mit nyer, ha pályázik?</h2>
                <ul>
                    <li>elismertséget egy komoly szakmai zsűri előtt</li>
                    <li> nyilvánosságot, népszerűséget mind a leendő megrendelők, mind a szakma előtt</li>
                    <li> még több ügyfelet, akik tudják és értik, akarják az Ön munkájának eredményét</li>
                </ul>
                <p>Mutassa meg Ön is a nyilvánosság előtt a kreativitását, azt, hogy hogyan válhat egy lakásból egy
                    igazán élvezhető és hosszútávon élhető otthon, hogyan képes a tudásával harmóniát teremteni.</p>


                <h4> Pályázzon Ön is – legyen 2016 az Ön éve!</h4>

                <br/>

                <b><a href="{{URL::to('/verseny/regisztracio')}}"> Regisztrálok a pályázatra most: Style&Draw az Én
                        pályázatom! </a></b>

                <br/>

                <h2>Amennyiben pályázik, Ön az alábbi nyeremények tulajdonosa lehet:</h2>

                <h3> Első helyezett díja:</h3>

                <p> A pályázatot támogató cégek fődíjként felajánlanak egy szakmai utat a 2016. évi 100% Design és
                    Decorex Kiállításokra Londonba. <b>A díj tartalmazza:</b></p>
                <ul>
                    <li> a repülőjegyet oda-vissza útra</li>
                    <li> 3 éjszaka szállást</li>
                    <li> napos szakmai belépőjegyet a kiállításokra</li>
                </ul>
                <h3> A Második helyezett díja:</h3>

                <p> A pályázatot támogató cégek felajánlanak egy több napos, gyárlátogatással egybekötött szakmai utat a
                    Porcelanosa gyárba Spanyolországba <br/> A díj tartalmazza az oda-vissza utat, a szállást, és a szakmai
                    programot</p>

                <h3>A Harmadik helyezett díja:</h3>

                <p> A pályázatot támogató cégek felajánlanak egy szakmai utat a soron következő, 2016 szeptemberében
                    megrendezésre
                    kerülő Cersai (Nemzetközi Burkolat és Fürdőszoba Felszerelés Expo) kiállításra Bolognába. <br/>
                    A díj tartalmazza az oda-vissza utat, a szállást, és a szakmai belépőjegyet a kiállításra.</p>

                <h3> Különdíjak:</h3>
                <ol>
                    <li>A pályázat szakmai védnöke, a Lakberendezők Országos Szövetsége, minden nyertesnek felajánl egy 1 éves díjmentes LOSZ tagságot.</li>
                    <li>A Cadline Kft. felajánl egy 1 éves előfizetést 408.000,- Ft+Áfa értékben az ArchLine.XP Professional belsőépítészeti tervező programra az általa kiválasztott pályamunka készítőjének.</li>
                    <li>A Top Trade Kft. felajánlja, hogy a termékeit legkreatívabban betervező pályázónak egy szakmai utat biztosít Nagy-Britanniába 2016 őszén.</li>
                    <li>A Forest Hungary Kft. az általa legszebbnek ítélt egyedi konyhabútor tervét készítő pályázót egy szakmai úttal egybekötött wellness hétvégével jutalmazza 2016 májusában. A díj tartalmaz: 2 éjszaka szállást két főre wellness szállodában, belépő jegyet a Göcseji Falumúzeumba, belépőt a Zalaegerszegi Termálfürdőbe és egy céglátogatást a Forest Hungary Kft. székhelyén megrendezésre kerülő Forest Expon.</li>
                    <li>A Forest Hungary Kft. az általa legszebbnek ítélt egyedi bútor tervet készítő pályázót egy szakmai úttal egybekötött wellness hétvégével jutalmazza 2016 májusában. A díj tartalmaz: 2 éjszaka szállást két főre wellness szállodában, belépő jegyet a Göcseji Falumúzeumba, belépőt a Zalaegerszegi Termálfürdőbe és egy céglátogatást a Forest Hungary Kft. székhelyén megrendezésre kerülő Forest Expon.</li>
                    <li>A GEO 96 Kft – KŐ-TÁR különdíja az általa legszebbnek ítélt természetes kőburkolat felületek tervezőjének egy gránátkővel díszített művészi ékszerkollekció, Bukits Anna kreatív ötvösművész egyedi alkotása.</li>
                    <li>A RAVAK Hungary Kft. az általa legszebbnek ítélt, termékeivel megtervezett fürdőszoba tervezőjét egy szegedi gyárlátogatással egybekötött wellness hétvégével jutalmazza 2016 májusában.
                    A díj tartalmazza: 2 éjszaka szállást két főre wellness szállodában és a RAVAK szegedi üzemében az öntött műmárvány termékek gyártásának megtekintését.</li>
                    <li>A Trilak Festékgyártó Kft. a festékeit a legkreatívabban, legegyedibb módon felhasználó
                        pályamunka készítőjének felajánl egy 100.000 Ft értékű festék-ajándékcsomagot.</li>
                    <li>Az Otti Manufactura Kft. felajánlja, hogy a termékeit legkreatívabban felhasználó pályázót vendégül látja székesfehérvári bemutatótermébe és gyár részlegébe, ahol is egészen közelről megismerkedhet azzal az öntömörödő betontechnológiával, amivel az Otti burkolatok készülnek. Valamint, lehetőséget biztosít számára, hogy saját kezűleg kipróbálhassa a beton öntését, egy egyedi tervezésű sablon segítségével.</li>

                </ol>




                <a class="btn btn-register btn-block" href="{{URL::to('/verseny/regisztracio')}}">Regisztrálok a
                    pályázatra!</a></p>

            </div>

        </div>
    </div>

@stop()