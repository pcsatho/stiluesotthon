{!! Form::open(array('class'=>'repeater','files'=>true)) !!}


<div class="form-group">
    {!!  Form::label('name','A projekt neve')  !!}
    {!!  Form::text('name',isset($entry) ? $entry->name : null ,array('required'=>'required','class'=>'form-control')) !!}
</div>
<div class="row">
    <div class="form-group col-xs-12 col-sm-6">
        {!!  Form::label('style','A pályamunkát meghatározó stílus')  !!}
        {!!  Form::select('style',\App\contestEntry::getStyles(),isset($entry) ? $entry->style : null ,array('required'=>'required','class'=>'form-control')) !!}
    </div>
    <div class="form-group col-xs-12 col-sm-6">
        {!!  Form::label('entry_category','Családmodell kiválasztása')  !!}
        {!!  Form::select('entry_category',\App\contestEntry::$entry_categories,isset($entry) ? $entry->entry_category : null,array('required'=>'required','class'=>'form-control')) !!}
    </div>
</div>
<div class="form-group">
    {!!  Form::label('info','A projekt rövid leírása')  !!}
    {!!  Form::textarea('info',isset($entry) ? $entry->info : null ,array('required'=>'required','class'=>'form-control')) !!}
    <span class="help-block"> Maximum 150 karakter </span>
</div>




@if(isset($entry))
    <div class="row">
        <div class="col-xs-12">
            {!!  Form::hidden('_unique', \Auth::user()->unique )!!}
            {!!  Form::hidden('_id', $entry->id )!!}
            <div class="col-xs-12">
                <span class="help-block"> Javasoljuk, hogy egyszerre csak néhány fájlt tallózzon be, és mentse el a projektet. <br> Elfogadott formátum:pdf. Maximális fájlméret 3 Megabájt. </span>
                <span class="help-block">Ha a korábban már feltöltött tervet újabbra szeretné cserélni, kérjük, egyszerűen a fájl kiválasztása gombra kattintva válassza ki a megfelelő rajzot. Ezzel a korábbi verziót lecseréli az oldal az újra.</span>
                <span class="help-block">Amennyiben egy rajzhoz több oldalt szeretne feltölteni, kérjük fűzze őket össze egy fájlba az <a
                            href="{!! \URL::to('files/pdf_osszefuzes.pdf') !!}">alábbi leírás</a>  alapján. </span>
                <span class="help-block">Excel-ből és Word-ből a <a
                            href="{!! \URL::to('files/hogyan_mentsd_el_excel_dokumentumodat_pdf_formatumba.pdf') !!}">alábbi
                        leírás </a>segítségével menthet PDF file-t.  </span>
            </div>
            @foreach(\App\contestEntry::$fileFields as $field=>$fieldName)
                <div data-mh="files" class="form-group col-xs-12 col-sm-6 col-md-4 col-lg-3">
                    {!!  Form::label($field,$fieldName)  !!}
                    {!!  Form::file($field,array('class'=>'form-input')) !!}

                    @if(!empty($entry->$field))
                        <a href="{!! \App\contestEntry::getUploadedFile($entry->id,$entry->$field) !!}">Jelenleg
                            feltöltött {!! $fieldName !!}</a>
                        {!!  Form::hidden('_'.$field,$entry->$field) !!}

                    @endif
                </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <div class="alert alert-danger">
                Amennyiben egy helységből többet is tervezett, a képek feltöltése és pályamunka elmentése után tudja
                beállítani azt, a <i><b>Hanyadik helyiség az adott kategóriában?</b></i> választólista segítségével.
            </div>
        </div>
    </div>
    <div id="rooms">
        <div class="contest-cover col-xs-12">
            @if(App\Http\Controllers\Contest\EntryImageController::hasEntryCover($entry->id))

                <h2>A pályamunka jelenlegi borítóképe</h2>
                <div class="img">
                    <img src="{!! App\Http\Controllers\Contest\EntryImageController::getEntryCover($entry->id) !!}"
                         alt="">
                </div>

            @endif
        </div>
        @foreach(\App\contestEntry::getRooms() as $roomSlug => $room)

            <div class="roomContainer col-xs-12">


                <h3>{!! $room  !!} </h3>
                <span class="help-block"> A kép maximális szélessége 2000px, magassága pedig 3000px lehet!</span>
                <span class="help-block"> Elfogadott formátum:jpg. Maximális fájlméret 3 Megabájt. </span>


                <div class="roomInner">
                    <div>
                        <div class="dropzone">

                            {!!  Form::hidden('category', $roomSlug ,array('class'=>'category'))!!}
                        </div>
                        <div class="category-cover mt-15 mb-15">
                            @if(App\Http\Controllers\Contest\EntryImageController::hasCategoryCover($entry->id,$roomSlug))

                                <h2>A kategória jelenlegi borítóképe</h2>
                                <div class="img">
                                    <img src="{!! App\Http\Controllers\Contest\EntryImageController::getCategoryCover($entry->id,$roomSlug) !!}"
                                         alt="">
                                </div>


                            @endif
                        </div>
                        <div class="preview dropzone-previews row">
                            @if($entry->images)
                                @foreach($entry->images as $image)

                                    @if($image->category === $roomSlug)
                                        <div class="previewContainer form-group row mt-15">
                                            <div class="col-xs-12">
                                                <div class="col-xs-12 col-sm-4 col-md-2 col-lg-1 image">
                                                    <img src="{!! \App\Http\Controllers\Contest\EntryImageController::getImage($image) !!}"
                                                         class="img-responsive center-block">
                                                    <button class="delImg btn btn-red btn-block"
                                                            data-image-id="{!!$image->id!!}">Kép törlése
                                                    </button>
                                                </div>

                                                <div class="col-xs-12 col-sm-8 col-md-10 col-lg-11 text">
                                                    <div>
                                                        <label for="contestImage[{!!$image->id!!}][name]">Kép
                                                            elnevezése</label>
                                                        <input class="form-control " type="text"
                                                               name="contestImage[{!!$image->id!!}][name]"
                                                               id="contestImage[{!!$image->id!!}][name] "
                                                               value="{!!$image->name!!}">
                                                    </div>

                                                    <div class="col-xs-6">
                                                        <label for="contestImage[{!!$image->id!!}][roomcount]">Hanyadik
                                                            helyiség az adott kategóriában?</label>
                                                        <br>
                                                        <select name="contestImage[{!!$image->id!!}][roomcount]"
                                                                id="contestImage[{!!$image->id!!}][roomcount]">

                                                            <option @if($image->roomcount == 1 || empty($image->roomcount)  ) selected="selected"
                                                                    @endif value="1">1
                                                            </option>
                                                            <option @if($image->roomcount == 2) selected="selected"
                                                                    @endif value="2">2
                                                            </option>
                                                            <option @if($image->roomcount == 3) selected="selected"
                                                                    @endif value="3">3
                                                            </option>

                                                        </select>
                                                    </div>
                                                    <div class="col-xs-6 text-right mt-15">
                                                        <button class="btn btn-info btn-small generate-entry-image"
                                                                data-entry-id="{{$entry->id}}"
                                                                data-image-id="{{$image->id}}">Pályázati borítókép
                                                            készítése
                                                        </button>
                                                        <button class="btn btn-info btn-small generate-category-image"
                                                                data-category="{{$roomSlug}}"
                                                                data-entry-id="{{$entry->id}}"
                                                                data-image-id="{{$image->id}}">Kategória borítókép
                                                            készítése
                                                        </button>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>


                                    @endif
                                @endforeach
                            @endif


                        </div>

                    </div>
                </div>


            </div>

        @endforeach
    </div>


@else
    <div class="alert alert-info col-xs-12">
        Fájlok feltöltéséhez kérjük mentse el a pályamunkáját!
    </div>
@endif


<div class="form-group  col-xs-12 mt-15">
    {!!  Form::submit('Pályamunka mentése',array('class'=>'btn btn-red')); !!}
</div>

{!! Form::close() !!}

@section('footerScripts')
    <script>


    </script>


    {!! JsValidator::formRequest('App\Http\Requests\contestEntryRequest') !!}



    <script>


    </script>
@endsection