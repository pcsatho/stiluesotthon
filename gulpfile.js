var elixir = require('laravel-elixir');
var iconfont = require('gulp-iconfont');
var runTimestamp = Math.round(Date.now() / 1000);
var consolidate = require('gulp-consolidate');
var rename = require("gulp-rename");
var gulp = require("gulp");
/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */


var fontName = "so-font";
gulp.task('fontCustom', function () {

    return gulp.src(['resources/assets/icons/*.svg']).pipe(iconfont({
            fontName: fontName, // required
            appendUnicode: false, // recommended option
            normalize: true, formats: ['ttf', 'eot', 'woff', 'woff2', 'svg'], // default, 'woff2' and 'svg' are available
            timestamp: runTimestamp // recommended to get consistent builds when watching files
        })).on('glyphs', function (glyphs) {

            var options = {

                glyphs: glyphs.map(function (glyph) {
                    // this line is needed because gulp-iconfont has changed the api from 2.0

                    return {name: glyph.name, codepoint: glyph.unicode[0].charCodeAt(0)}
                }), fontName: fontName, fontPath: '../font/so/', // set path to font (from your CSS file if relative)
                className: 'so-icon' // set class name in your CSS
            };
            gulp.src('resources/assets/templates/template.css').pipe(consolidate('lodash', options)).pipe(rename({basename: fontName})).pipe(gulp.dest('resources/assets/sass/')); // set path to export your CSS


        }).pipe(gulp.dest("public/assetts/font/so/"));
});



elixir(function(mix) {
    mix.sass('frontend/app.scss','public/assetts/frontend/app.css');


    mix.sass('backend/app.scss','public/assetts/backend/app.css');


    mix.scripts(
        [
        '../../../node_modules/bootstrap-sass/assets/javascripts/bootstrap.js',
        '../../../bower_components/matchheight/jquery.matchHeight.js',
        'frontend/app.js'
        ]
        , 'public/assetts/frontend/js/app.js')
        .scripts([

            /*'../../../bower_components/jquery/dist/jquery.js',*/
            '../../../bower_components/admin-lte/bootstrap/js/bootstrap.js',
            '../../../bower_components/cropper/dist/cropper.js',
            '../../../bower_components/matchheight/jquery.matchHeight.js',
            '../../../bower_components/summernote/dist/summernote.js',
            '../../../bower_components/summernote/lang/summernote-hu-HU.js',
            '../../../bower_components/dropzone/dist/dropzone.js',
            '../../../vendor/proengsoft/laravel-jsvalidation/public/js/jsvalidation.js',
            'backend/app.js'
        ], 'public/assetts/backend/js/app.js').scripts([

            'admin/app.js'], 'public/assetts/admin/js/app.js');


    mix.browserSync({
     proxy: 'stilusesotthon.dev'
    });
    mix.task('fontCustom');

});
