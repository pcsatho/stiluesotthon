@extends('frontend.layouts.default')
@section('title')
   Regisztráció - Stilus és Otthon
@stop
@section('content')
   <div class="container">
       <div class="row">
           <div class="col-xs-8 col-xs-push-2 col-md-6 col-md-push-3">
               <form method="POST" action="{{URL::to('/verseny/regisztracio')}}">
                   {!! csrf_field() !!}
                   @if (count($errors) > 0)
                       <div class="alert alert-danger">
                       <ul>
                           @foreach ($errors->all() as $error)
                               <li>{{ $error }}</li>
                           @endforeach

                       </ul>
                       </div>
                   @endif

                   {!!
                   \Bootstrapper\Facades\ControlGroup::generate(
                   \Bootstrapper\Facades\Form::label('name', 'Név'),
                   \Bootstrapper\Facades\Form::text('name'),
                   \Bootstrapper\Facades\Form::help('Az Ön neve')
                   )

                   !!}

                   {!!
                   \Bootstrapper\Facades\ControlGroup::generate(
                       \Bootstrapper\Facades\Form::label('email', 'E-mail'),
                       \Bootstrapper\Facades\Form::email('email'),
                       \Bootstrapper\Facades\Form::help('Az Ön e-mail címe')
                   )

                   !!}


                   {!!
                   \Bootstrapper\Facades\ControlGroup::generate(
                   \Bootstrapper\Facades\Form::label('password', 'Jelszó'),
                   \Bootstrapper\Facades\Form::password('password')


                   )

                   !!}
                   {!!
                   \Bootstrapper\Facades\ControlGroup::generate(
                   \Bootstrapper\Facades\Form::label('password_confirmation', 'Jelszó újra'),
                   \Bootstrapper\Facades\Form::password('password_confirmation'),
                   \Bootstrapper\Facades\Form::help('Tanácsoljuk, hogy jelszava tartalmazzon kis-és nagybetűket, illetve számokat')


                   )

                   !!}

{{--                   {!!
                   ControlGroup::generate(
                   \Bootstrapper\Facades\Form::label('aszf', 'Elfogadom az Általános szerződési feltételeket!'),
                   \Bootstrapper\Facades\Form::checkbox('aszf'),
                   \Bootstrapper\Facades\Form::help('<a target="_blank" href="'.asset('files/aszf.pdf').'">Az ÁSZF elfogadása kötelező!</a> (<a target="_blank" href="'.asset('files/118539.pdf').'">NAIH határozat</a>)')
                   )
                   !!}--}}



                   <div>
                       <button class=" btn btn-primary btn-block btn-lg" type="submit">Regisztráció</button>
                   </div>
               </form>
           </div>
       </div>
   </div>
@stop()

