jQuery(document).ready(function () {


	$(function(){

		$('.generate-entry-image').on('click',function(e){
			e.preventDefault();
			e.defaultPrevented;
			var entryID = $(this).attr('data-entry-id');
			var imageID = $(this).attr('data-image-id');
			var $this = $(this);

			console.log(entryID,imageID);
			var $this = $(this);
			$.ajax({
				url: '/verseny/panel/palyazati-anyagok/generate-entry-cover/'+entryID+'/'+imageID,
				type: 'POST',
				data:{
					'_token': $('[name=_token]').val(),

				},
				success: function (result) {
					if(result.status == true){


						$this.parents('#rooms').find('.contest-cover .img').html('<img src="'+result.src+'">');
						alert(result.message);
					}else{
						alert(result.message);
					}

				}
			});

		});



		$('.generate-category-image').on('click',function(e){
			e.preventDefault();
			e.defaultPrevented;
			var entryID = $(this).attr('data-entry-id');
			var imageID = $(this).attr('data-image-id');
			var category = $(this).attr('data-category');
			var $this = $(this);

			console.log(entryID,imageID);
			var $this = $(this);
			$.ajax({
				url: '/verseny/panel/palyazati-anyagok/generate-category-cover/'+entryID+'/'+category+'/'+imageID,
				type: 'POST',
				data:{
					'_token': $('[name=_token]').val(),

				},
				success: function (result) {
					if(result.status == true){


						$this.parents('.roomInner').find('.category-cover .img').html('<img src="'+result.src+'">');
						alert(result.message);
					}else{
						alert(result.message);
					}

				}
			});

		});


	});

	$(function () {

		$('textarea').summernote({
			lang: 'hu-HU',
			minHeight: 300,
			height: 300,
			toolbar: [// [groupName, [list of button]]
				['style',
				 ['bold', 'italic', 'underline', 'clear']
				]
			],
			callbacks : {
				onPaste: function (e) {
					var bufferText = ((e.originalEvent || e).clipboardData || window.clipboardData).getData('Text');
					e.preventDefault();
					setTimeout(function(){

						document.execCommand('insertText', false, bufferText);
					},10);

				}
			}
		});

	});

	$(function () {
		var $smallImage = $('#smallProfilePic'), smallImageoriginalData = {};
		$smallImage.cropper({
			aspectRatio: 1, minContainerHeight: 400, minContainerWidth: 500, cropend: function (e) {
				smallImageoriginalData = $smallImage.cropper("getCroppedCanvas");
				$('#smallProfilePicBase').attr('value', smallImageoriginalData.toDataURL());

			}, zoom: function (e) {
				smallImageoriginalData = $smallImage.cropper("getCroppedCanvas");
				$('#smallProfilePicBase').attr('value', smallImageoriginalData.toDataURL());

			}
		});
		var $croppedImage = $('#croppedProfilePic'), croppedImageoriginalData = {};
		$croppedImage.cropper({
			aspectRatio: 460 / 280, minContainerHeight: 400, minContainerWidth: 500, cropend: function (e) {

				croppedImageoriginalData = $croppedImage.cropper("getCroppedCanvas");

				$('#croppedProfilePicBase').attr('value', croppedImageoriginalData.toDataURL());

			},

			zoom: function (e) {

				croppedImageoriginalData = $croppedImage.cropper("getCroppedCanvas");
				$('#croppedProfilePicBase').attr('value', croppedImageoriginalData.toDataURL());

			}

		});
	});

	$(function(){
		var dzOptions = {
			parallelUploads:1,
			url: "/verseny/panel/palyazati-anyagok/upload-image",
			maxFilesize: 3,
			addRemoveLinks: true,
			maxFiles: 9,
			init: function () {
				this.on("addedfile", function (file) {
					file.previewElement.addEventListener("click", function () {

					});
				});
				this.on("success", function (file, xhr) {

					var img = $(file.previewElement.innerHTML).find('img');

					var html = $('<div class="previewContainer form-group row mt-15" ><div class="col-xs-12">' + '<div class="col-xs-12 col-sm-4 col-md-2 col-lg-1 image">' + '<img src="' + img[0].currentSrc + '" class="img-responsive center-block"> ' + '<button class="delImg btn btn-red btn-block" data-image-id =' + xhr.id + '>Kép törlése</button>' + '</div>' + '<div class="col-xs-12 col-sm-8 col-md-10 col-lg-11 text">' + '<label for="contestImage[' + xhr.id + ']" >Kép elnevezése </label>' + '<input class="form-control " type="text" name="contestImage[' + xhr.id + ']" id="contestImage[' + xhr.id + '] "value="' + xhr.category + '">' + '</div></div>');
					$($($(this)[0].element).siblings('.preview')[0]).append(html);
					this.removeFile(file);
				});


				this.on("error", function (file, response) {
					// do stuff here.
					/*alert(response);*/

				});

				/*console.log($($(this)[0].element).siblings('.preview')[0])*/
			},
			sending: function (file, xhr, formData) {

				formData.append("_token", $('[name=_token]').val());
				formData.append("path", $('[name=_unique]').val());
				formData.append("id", $('[name=_id]').val());
				formData.append("category", $($(this)[0].element).find('.category').val());
			},
			success: function (file, xhr) {


			},
			dictDefaultMessage: 'Feltöltéshez kattintson, vagy dobja ide a fájlokat!',
			dictFallbackMessage:'Az Ön böngészője nem támogatja a Drag&Drop funkciót',
			dictFallbackText:'Kérjük használja a lent található formot!',
			dictInvalidFileType:'Hibás fájlformátum',
			dictFileTooBig : 'A fájl túl nagy',
			dictResponseError : 'Hiba - státusz : {{statusCode}}',
			dictCancelUpload: 'Feltöltés megszakítva',
			dictCancelUploadConfirmation:'Biztosan megszakítja a feltöltést?',
			dictRemoveFile:'Fájl törlése',
			dictMaxFilesExceeded:'Nem tölthet fel több fájlt!'
		}
		$(".dropzone").dropzone(dzOptions);

	$('body').on('click','.delImg',function(e){
		e.preventDefault();
		e.defaultPrevented;
		 var id = $(this).attr('data-image-id');
		var $this = $(this);
		$.ajax({
			url: '/verseny/panel/palyazati-anyagok/delete-image/'+id,
			type: 'DELETE',
			data:{'_token': $('[name=_token]').val()},
			success: function (result) {
				if(result.status == 'success'){


					$this.parents('.previewContainer').slideUp(300,function(){
						$this.parents('.previewContainer').remove();
					});
				}

			}
		});





	});

/*		var $clonecontainer = $('.repeat');
		$clonecontainer.cloneya({
			minimum: 1,
			maximum: 3,
			cloneThis: '.roomContainer',
			valueClone: true,
			dataClone: false,
			deepClone: false,
			cloneButton: '.add',
			deleteButton: '.delete',
			clonePosition: 'after',
			serializeID: true,
			ignore: 'div.preview',
			preserveChildCount: false
		});

		$clonecontainer.on('maximum.cloneya', function (event, maximumCount, toClone) {
			alert("Egy helyiségből nem lehet több " + maximumCount + " darabnál.");
		});
		$clonecontainer.on('after_append.cloneya', function (event,toClone, newClone) {
			console.log();
			var $conts = $(event.target).find('.roomContainer');

			$.each($conts,function($key,cont){
				$key = parseInt($key)+1;
				if($key+1 > 1){
				$(cont).find('h3 > span').html($key);
				}
			});

			$(newClone).find('.dropzone').dropzone(dzOptions);
		});*/
	/*	$('.repeater').repeater({
			// (Optional)
			// "defaultValues" sets the values of added items.  The keys of
			// defaultValues refer to the value of the input's name attribute.
			// If a default value is not specified for an input, then it will
			// have its value cleared.
			defaultValues: {
				'text-input': 'foo'
			}, // (Optional)
			// "show" is called just after an item is added.  The item is hidden
			// at this point.  If a show callback is not given the item will
			// have $(this).show() called on it.
			show: function () {
				$(this).slideDown();
			}, // (Optional)
			// "hide" is called when a user clicks on a data-repeater-delete
			// element.  The item is still visible.  "hide" is passed a function
			// as its first argument which will properly remove the item.
			// "hide" allows for a confirmation step, to send a delete request
			// to the server, etc.  If a hide callback is not given the item
			// will be deleted.
			hide: function (deleteElement) {
				if (confirm('Are you sure you want to delete this element?')) {
					$(this).slideUp(deleteElement);
				}
			}, // (Optional)
			// You can use this if you need to manually re-index the list
			// for example if you are using a drag and drop library to reorder
			// list items.
			// Removes the delete button from the first list item,
			// defaults to false.
			isFirstItemUndeletable: true
		})*/



		$('input[type="file"]').on('blur',function(){
			$.fn.matchHeight._update();
		})


	});

$(function(){
	$('.finalizeEntry').click(function(e){
		var r = confirm("Press a button!");
		if (r == true) {
			txt = "You pressed OK!";
		} else {
			e.preventDeafult();
			e.defaultPrevented;
		}
	});
});

});