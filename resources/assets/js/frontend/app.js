jQuery(document).ready(function($){

    $(function(){
        var bodyHeight = $('body').height();
        var windowHeight = $(window).height();

            if(bodyHeight < windowHeight){
                $('body').height(windowHeight);
                $('html').height(windowHeight);
            }else{
                $('body').height(bodyHeight);
                $('html').height(bodyHeight);
            }
    });


    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })


});