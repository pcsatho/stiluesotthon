<?php

namespace App\Http\Controllers\Contest;

use App\contestEntry;
use App\contestImage;
use App\Http\Controllers\Controller;
use App\User;

class EntryImageController extends Controller
{


    public static function hasCategoryCover($entryID, $categoryName)
    {
        $user = \Auth::user();
        $coverFile = public_path() . '/entries/' . $user->unique . '/' . $entryID . '/' . $categoryName . '/cover.jpg';
        if (\File::exists($coverFile)) {
            return true;
        }
        return false;


    }


    public static function hasEntryCover($entryID)
    {

        $user = \Auth::user();
        $coverFile = public_path() . '/entries/' . $user->unique . '/' . $entryID . '/' . 'cover.jpg';
        if (\File::exists($coverFile)) {
            return true;
        }
        return false;
    }

    public static function getEntryCover($entryID,$user=null)
    {

        if(!$user){
        $user = \Auth::user();
        }else{
            $user = User::find($user);
        }


        $path = '/entries/' . $user->unique . '/' . $entryID . '/' . 'cover.jpg';
        $coverFile = \URL::to('/') . $path;
        if(\File::exists(public_path().$path)){

            $coverFile = \URL::to('/').$path.'?'.filemtime(public_path().$path);
        }


        return $coverFile;
    }

    public static function getCategoryCover($entryID,$category)
    {

        $user = \Auth::user();


        $path = '/entries/' . $user->unique . '/' . $entryID . '/'. $category.'/cover.jpg';
        $coverFile = \URL::to('/') . $path;

        if(\File::exists(public_path().$path)){

            $coverFile = \URL::to('/').$path.'?'.filemtime(public_path().$path);
        }

        return $coverFile;
    }


    public static  function getImage($image)
    {


        $path = '/entries/'.$image->src;
        $return =  \URL::to($path);

        if(\File::exists(public_path().$path)){

            $return = \URL::to('entries/'.$image->src.'?'.filemtime(public_path().'/entries/'.$image->src));
        }

        return $return;


    }


    public static function getFrontendCategoryCover($entryID,$category)
    {


        $entry = contestEntry::find($entryID);
        $coverFileURL = \URL::to('/') . '/entries/' . $entry->user->user->unique . '/' . $entryID . '/'. $category.'/cover.jpg';
        $coverFilePath = public_path().'/entries/' . $entry->user->user->unique . '/' . $entryID . '/'. $category.'/cover.jpg';

        if(!\File::exists($coverFilePath)){
            $imgPath = public_path('entries/').$entry->images()->where('category','=',$category)->first()->src;
            \Image::make($imgPath)->fit(600, 250)->save($coverFilePath);
        }

        return $coverFileURL;
    }


    public function makeCategoryCover($entryID, $categoryName, $imageID)
    {

        $user = \Auth::user();
        $userDir = public_path() . '/entries/' . $user->unique . '/' . $entryID . '/' . $categoryName . '/';
        $coverFile = $userDir . 'cover.jpg';
        $coverURL =  \URL::to('/') . '/entries/' . $user->unique . '/' . $entryID . '/' . $categoryName . '/'.'cover.jpg';
        if (\File::isDirectory($user)) \File::makeDirectory($userDir);

        $image = contestImage::find($imageID);
        $imageSRC = public_path() .'/entries/' .$image->src;

        $coverImage = \Image::make($imageSRC)->fit(600, 250);

        if ($coverImage->save($coverFile)) {
            return \Response::json(array(
                'status' => TRUE,
                'message' => 'A Borítókép mentése sikeres!',
                'src' => $coverURL

            ));
        }
        return \Response::json(array(
            'status' => FALSE,
            'message' => 'A Borítókép mentése sikertelen!'
        ));


    }


    public function makeEntryCover($entryID, $imageID)
    {

        $user = \Auth::user();
        $userDir = public_path() . '/entries/' . $user->unique . '/' . $entryID . '/';
        $coverFile = $userDir . 'cover.jpg';
        $coverURL =  \URL::to('/') . '/entries/' . $user->unique . '/' . $entryID . '/'.'cover.jpg';
        if (\File::isDirectory($user)) \File::makeDirectory($userDir);

        $image = contestImage::find($imageID);
        $imageSRC = public_path() .'/entries/' .$image->src;

        $coverImage = \Image::make($imageSRC)->widen(400);

        if ($coverImage->save($coverFile)) {
            return \Response::json(array(
                'status' => TRUE,
                'message' => 'A Borítókép mentése sikeres!',
                'src' => $coverURL

            ));
        }
        return \Response::json(array(
            'status' => FALSE,
            'message' => 'A Borítókép mentése sikertelen!'
        ));


    }


}