<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateZsuriTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('zsuri', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug')->index()->unique();
            $table->string('title')->nullable();
            $table->text('goals')->nullable();
            $table->string('image');
            $table->text('about');
            $table->string('phone')->nullable();
            $table->string('mail')->nullable();
            $table->string('web')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('zsuri');
    }
}
