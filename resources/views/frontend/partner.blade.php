@extends('frontend.layouts.default')
@section('title')
    Partnereink - Stilus és Otthon
@stop
@section('content')
<div class="container">
    <div >
        @foreach($partners as $partner)
        <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 partner-item {{$partner->slug}}">
            <a title="{{$partner->name}}" href="{{URL::to('partnerek/'.$partner->slug)}}">


            <img class="img-responsive center-block" src="{{asset('images/partner/'.$partner->logo)}}" alt="{{$partner->name}}"/>
            </a>
        </div>
        
        @endforeach

    </div>
</div>

  <div class="container">
      <div id="losz">
          <div class="col-xs-12 col-lg-4">
              <a title="Lakberendezők országos szövetsége" href="http://stilusesotthon.hu/partnerek/lakberendezok-orszagos-szoevetsege">
              <img src="{{asset('/images/losz_logo.jpg')}}" alt="Lakberendezők országos szövetsége"
                   class="img-responsive center-block"/>
              </a>
          </div>
      </div>
  </div>
@stop()