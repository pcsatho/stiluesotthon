<?php

namespace App\Http\Controllers\Contest;

use App\contestImage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

class UploadController extends Controller {

	public function uploadImage()
	{

		if ( Request::ajax() )
		{



			$file = array('image' => Input::file('file'));
			// setting up rules
			$rules = array('image' => 'mimes:jpeg,jpg'); //mimes:jpeg,bmp,png and for max size max:10000
			// doing the validation, passing post data, rules and the messages
			$validator = \Validator::make($file, $rules);

			if ($validator->fails()) {

				// send back to the page with the input data and errors
				return response()->json($validator->errors()->first(), 400);
			}

			$file           = Input::file('file');
			$image = \Image::make($file);
			$width = $image->width();
			$height = $image->height();
			$size = $image->filesize() / 1024;

			if($width > 2000 || $height > 3000){
				return response()->json('A kép maximális szélessége 2000px, magassága pedig 3000px lehet!', 400);
			}
			if($size > 3000){
				return response()->json('A kép maximális mérete 3 Megabájt lehet', 400);
			}

			$path           = Input::get('path');
			$category       = Input::get('category');
			$id             = Input::get('id');
			$directory      = public_path() . '/entries/' . $path . '/' . $id . '/' . $category . '/';
			$sizesDirectory = $directory . 'sizes/';
			$count          = (int) count(\File::files($directory));
			$maximumCount   = ( $category != 'moodboard' ) ? 9 : 10;

			if ( $count >= $maximumCount )
			{
				return response()->json('Maximum 9 kép/helyiség típus  megengedett!', 400);
			}
		}
		if ( ! \File::isDirectory($directory) )
		{
			\File::makeDirectory($directory, 0775, true);
		}
		if ( ! \File::isDirectory($sizesDirectory) )
		{

			\File::makeDirectory($sizesDirectory, 0775, true);
		}
		$destinationPath = $directory;
		$filename        = $count + 1 . '.' . $file->getClientOriginalExtension();
		$image           = \Image::make($file)->resize(3000, null, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		$image_large          = \Image::make($file)->resize(1800, null, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		});
		$image_large_filename = 'large_' . ( $count + 1 ) . '.' . $file->getClientOriginalExtension();

		$image_thumb          = \Image::make($file)->resize(470, null, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		})->crop(460, 280);
		$image_thumb_filename = 'thumb_' . ( $count + 1 ) . '.' . $file->getClientOriginalExtension();

		$image_medium = \Image::make($file)->resize(600, null, function ($constraint)
		{
			$constraint->aspectRatio();
			$constraint->upsize();
		});

		$image_medium_filename = 'medium_' . ( $count + 1 ) . '.' . $file->getClientOriginalExtension();

		$upload_success = $image->save($destinationPath . $filename, 80);

		if ( $upload_success )
		{
			$image_large->save($sizesDirectory . $image_large_filename, 80);
			$image_medium->save($sizesDirectory . $image_medium_filename, 80);
			$image_thumb->save($sizesDirectory . $image_thumb_filename, 80);
			$response = array(
				'src'      => $path . '/' . $id . '/' . $category . '/' . $filename,
				'category' => $category,
				'status'   => 'success'
			);
			$imageID  = $this->saveToDatabase($id, $response['src'], null, $category);
			if ( $imageID )
			{
				$response['id'] = $imageID;

				return response()->json($response, 200);
			}
			else
			{
				return response()->json('error', 400);
			}
		}
		else
		{
			return response()->json('error', 400);

		}
	}


	public function saveToDatabase($id, $src, $name, $category)
	{

		$image                   = new contestImage();
		$image->contest_entry_id = $id;
		$image->name             = ! empty( $name ) ? $name : $category;
		$image->category         = $category;
		$image->src              = $src;
		if ( $image->save() )
		{
			return $image->id;
		}
		else
		{
			return false;
		}


	}


	public function deleteImage($id)
	{
		if ( Request::ajax() )
		{
			$image = contestImage::find($id);
			if ( $this->removeFromDatabase($id) )
			{
				$file = public_path() . '/entries/' . $image->src;
				$sizesDir = explode('/',$file);
				$filename = array_pop($sizesDir);
				$sizesDir = implode('/',$sizesDir);


				\File::delete($file);
				\File::delete($sizesDir.'/sizes/thumb_'.$filename);
				\File::delete($sizesDir . '/sizes/medium_' . $filename);
				\File::delete($sizesDir . '/sizes/large_' . $filename);

				return response()->json(array( 'status' => 'success' ), 200);

			}
			else
			{
				return response()->json(array( 'status' => 'error' ), 400);
			}


		}

	}


	public function removeFromDatabase($id)
	{

		$image = contestImage::find($id);

		if($image):
			return $image->delete();
		else:
			return redirect()->back()->withErrors('A kép törlése sikertelen!');
		endif;
	}

}