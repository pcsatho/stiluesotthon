@extends('backend.dashboard')

@section('content')

    <h2>
        Pályamunkák
    </h2>
    @include('backend._partials.message')


    @if($entries->count() == 0)
       <div class="col-xs-12">
           <div class="alert alert-info">
               Kérjük kattintson az új hozzáadása gombra, adja meg a pályázat alapvető adatait, majd mentse el azokat. Ezután a módosításra kattintva tudja folytatni a tervek feltöltését
           </div>
       </div>
    @endif
    <div class="col-xs-12">

        <a class="btn btn-red pull-right " href="{!!URL::route('getUpload')!!}"><i class="fa fa-plus"></i> Új hozzáadása</a>
    </div>



    <div class="col-xs-12 mt-15">
        <div class="responsive-table">
        <table class="table table-striped">
            <tr>
                <th>Név</th>
                <th>Info</th>

                <th></th>
            </tr>
            @foreach($entries as $index=>$entry)
                <tr>
                    <td>{!!$entry->name !!}</td>
                    <td>{!! str_limit($entry->info,30).'...'  !!}</td>

                    <td class="pull-right">
                        <a class="btn btn-success btn-mini btn-inline" href="{!!URL::route('getEditUpload',array('id'=>$entry->id))!!}">Módosítás</a>
                        {!! Form::open(array('class'=>"inline",'route' => array('getDeleteUpload', $entry->id), 'method' => 'delete'))
                        !!}
                        <button type="submit" class="btn btn-danger btn-mini btn-inline">Törlés</button>
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>

    </div>
    </div>

@endsection