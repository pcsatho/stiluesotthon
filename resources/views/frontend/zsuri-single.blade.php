@extends('frontend.layouts.default')
@section('title')
    {{$zsuri->name}}  - Stilus és Otthon
@stop
@section('content')
<div id="zsuri-single" class="container">
    <div class="row">
        <div class="header">
            <div data-mh="zsuri" class="col-xs-12 col-sm-2 ">
                <img src="{{'/images/zsuri/'.$zsuri->image}}" alt="{{$zsuri->name}}" class="img-responsive center-block"/>
            </div>
            <div class="details col-xs-12 col-sm-8" data-mh="zsuri" >
                <div class="name"><h1>{{$zsuri->name}}</h1></div>
                <div class="title"><h2>{{$zsuri->title}}</h2></div>
                <div class="goals">
                    @if(!empty($zsuri->goals ))
                        {!! $zsuri->goals !!}@endif
                </div>


            </div>
        </div>
        <div class="about col-xs-12 col-sm-8 col-sm-push-2">

            {!! $zsuri->about !!}

            @if(!empty($zsuri->url ))
                <a target="_blank" class="btn btn-primary" data-trigger="hover focus" data-toggle="tooltip" data-placement="top"
                   title="{{$zsuri->name}} weboldala elérhető a {{$zsuri->url}} linken" href="{!! $zsuri->url !!}">{!! $zsuri->name !!} weboldala</a>@endif
        </div>
    </div>
</div>
@stop()