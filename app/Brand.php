<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model {

    protected $table = 'brands';


	public function partner() {
		return $this->hasOne('App\Partner');
	}
}
