<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveContactFromZsuri extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('zsuri', function (Blueprint $table) {
            $table->dropColumn('phone');
            $table->dropColumn('mail');
            $table->dropColumn('web');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('zsuri', function (Blueprint $table) {
            $table->string('phone')->nullable();
            $table->string('mail')->nullable();
            $table->string('web')->nullable();
        });
    }
}
