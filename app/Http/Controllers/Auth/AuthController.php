<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    protected $redirectTo = '/verseny/panel';
    protected $loginPath = 'verseny/bejelentkezes';

    protected $redirectPath = '/verseny/panel';
    protected $username = 'unique';

    private $maxLoginAttempts = 5;

    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [

            'email' => 'required|email|max:255|unique:users',
            'name' => 'required',
            'password' => 'required|confirmed|min:6',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
       $user =  User::create([
            'name'=> $data['name'],
            'unique'=>Str::random(16),
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);

        Mail::send('emails.welcome', [ 'user' => $user ], function ($m) use ($user)
        {
            $m->from('rendszer@stilusesotthon.hu','Style&Draw rendszer');
            $m->to($user->email, $user->name)->subject('Style&Draw verseny regisztráció');
        });

        return $user;
    }


    public function authenticate()
    {
        if ( Auth::attempt([ 'unique' => $unique, 'password' => $password ]) )
        {
            // Authentication passed...
            return redirect()->intended('verseny/panel');
        }
    }


    public function postRegister(\Illuminate\Http\Request $request)
    {
        $validator = $this->validator($request->all());

        if ( $validator->fails() )
        {
            $this->throwValidationException($request, $validator);
        }

        \Auth::login($this->create($request->all()));

        return redirect(\URL::to('koszonjuk-a-regisztraciot'));
    }

}
