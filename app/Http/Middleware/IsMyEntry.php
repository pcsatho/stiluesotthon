<?php namespace App\Http\Middleware;

// Custom Service for the App's particular needs

use Closure;
use Request;

class IsMyEntry
{

    protected $entry;

    public function __construct(\App\contestEntry $entry)
    {
        $this->entry = $entry;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $entry_id = Request::route()->parameter('id', null);

        $entry = $this->entry->findOrFail($entry_id);

        if (\Auth::user()->contestant->id === $entry->contest_user_id) {
            return $next($request);
        }

        $message = 'A kért erőforrás nem megjeleníthető';
        return redirect()->back()->withErrors($message);


    }
}