@extends('frontend.layouts.default')

@section('title')
    Gyakori kérdések - Stilus és Otthon
@stop

@section('content')

    <div class="container">
        <div class="row">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">


                @foreach($faq as $index=>$item)
                    <div class="panel panel-transparent">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse{{$index}}"
                                   aria-expanded="true" aria-controls="collapse{{$index}}">
                                    {{$item->question}}
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{$index}}" class="panel-collapse collapse {{$index == 0 ? 'in' : ''}}"
                             role="tabpanel" aria-labelledby="heading{{$index}}">
                            <div class="panel-body">
                                {!!$item->answer!!}
                            </div>
                        </div>
                    </div>
                    <h2></h2>

                @endforeach
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">

                       <p><b> <a target="_blank" href="{{asset('files/palyazati_kiiras.pdf')}}">Olvassa el a pályázati kiírást,
                                    amelyben a legtöbb kérdésére találhat választ!</a></b></p>

                <p>A felmerülő kérdésekkel folyamatosan bővítjük tárunkat.</p>

                <a class="btn btn-primary" data-trigger="click" data-toggle="tooltip" data-placement="top"
                   title="Írjon nekünk a lakberendezo@styleandhome.hu e-mail címre"
                   href="mailto:lakberendezo@styleandhome.hu">
                    Tegye fel Ön is kérdését</a>


            </div>
        </div>
    </div>
@stop()




