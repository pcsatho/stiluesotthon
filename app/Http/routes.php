<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'Bug\PagesController@getIndex');
Route::get('/newhome', ["uses"=>'Bug\PagesController@getIndex', 'as'=>'home']);
Route::get('/palyazat', 'Bug\PagesController@getPalyazat');
Route::get('/zsuri', 'Bug\PagesController@getJuries');
Route::get('/zsuri/{slug}', 'Bug\PagesController@getJury');
Route::get('/partnerek', 'Bug\PagesController@getPartners');
Route::get('/partnerek/{slug}', 'Bug\PagesController@getPartner');
Route::get('/partnerek/losz', 'Bug\PagesController@getLOSZPartner');
Route::get('/kapcsolat', 'Bug\PagesController@getContact');
Route::get('/faq', 'Bug\PagesController@getFAQ');
Route::get('/aszf', 'Bug\PagesController@getASZF');
Route::get('/koszonjuk-a-regisztraciot', 'Bug\PagesController@getThankyou');



// Authentication routes...
Route::get('verseny/bejelentkezes', 'Auth\AuthController@getLogin');
Route::post('verseny/bejelentkezes', 'Auth\AuthController@postLogin');
Route::get('kijelentkezes', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('verseny/regisztracio', 'Auth\AuthController@getRegister');
Route::post('verseny/regisztracio', 'Auth\AuthController@postRegister');

//PW reset
Route::get('elfelejtett-jelszo', 'Auth\PasswordController@getEmail');
Route::post('elfelejtett-jelszo', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('elfelejtett-jelszo/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('elfelejtett-jelszo/reset', 'Auth\PasswordController@postReset');

//verseny panel routes
Route::group([ 'middleware' => 'auth' ], function ()
{
	Route::group([ 'prefix' => 'verseny/panel' ], function ()
	{
		Route::get('/', 'Contest\BackendController@getPanel');
		Route::get('tervezesi-segedletek', 'Contest\BackendController@getHelp');

		Route::get('profil', ['uses'=>'Contest\BackendController@getProfile','middleware'=>'App\Http\Middleware\voteEnd']);
		Route::post('profil', ['uses'=>'Contest\EntryUserController@postProfile','middleware'=>'App\Http\Middleware\voteEnd']);


		Route::group([ 'prefix' => 'palyazati-anyagok','middleware'=>'App\Http\Middleware\voteEnd' ], function ()
		{

			Route::post('upload-image', [
					'as'   => 'uploadImage',
					'uses' => 'Contest\UploadController@uploadImage'
			]);



			Route::delete('delete-image/{id}', [
				'as'   => 'deleteImage',
				'uses' => 'Contest\UploadController@deleteImage'
			]);

			Route::get('/', [
				'as'   => 'getUploadIndex',
				'uses' => 'Contest\EntryController@getUploadIndex'
			]
			);
			Route::get('uj', [
					'as'   => 'getUpload',
					'uses' => 'Contest\EntryController@getUpload'
				]
			);

			Route::get('veglegesites', [
					'as'   => 'finalizeEntries',
					'uses' => 'Contest\EntryController@finalizeEntries'
				]);

			Route::post('uj', [
					'as'   => 'postUpload',
					'uses' => 'Contest\EntryController@postUpload'
				]);

			Route::get('szerkesztes/{id}', [
					'as'   => 'getEditUpload',
					'uses' => 'Contest\EntryController@getEditUpload',
					'middleware' => 'ismyentry'
				]);

			Route::post('szerkesztes/{id}', [
				'as'   => 'postEditUpload',
				'uses' => 'Contest\EntryController@postEditUpload',
					'middleware' => 'ismyentry'
			]);


			Route::delete('torles/{id}', [
				'as'   => 'getDeleteUpload',
				'uses' => 'Contest\EntryController@getDeleteUpload',
					'middleware' => 'ismyentry'
			]);

			Route::post('generate-entry-cover/{entryID}/{imageID}', [
					'as'   => 'generateEntryCover',
					'uses' => 'Contest\EntryImageController@makeEntryCover'
			]);
			Route::post('generate-category-cover/{entryID}/{category}/{imageID}', [
					'as'   => 'generateCategoryCover',
					'uses' => 'Contest\EntryImageController@makeCategoryCover'
			]);

		});

	});
});



//Vote Routes



Route::get('bejelentkezes/social/redirect/{provider}', ['uses' => 'Auth\SocialAuthController@redirectToProvider', 'as' => 'social.login']);
Route::get('bejelentkezes/social/{provider}', 'Auth\SocialAuthController@handleProviderCallback');
Route::get('bejelentkezes','Bug\PagesController@getLogin');
