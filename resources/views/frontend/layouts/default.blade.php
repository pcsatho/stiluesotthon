<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>@yield('title', 'Stílus és Otthon')</title>

    <meta name="description" content="    @yield('meta-description')">
    <meta name="viewport" content="width=device-width">
    <meta name="author" content="BugTV">
    <meta name="robots" content="index, follow">
    <meta name="revisit-after" content="1 days">
    <meta property="og:title" content="@yield('title', 'Stílus és Otthon')">
    <meta property="og:type" content="website">
    <meta property="og:url" content="{{URL::to('/')}}">
    <meta property="og:image" content="{{asset('/assetts/frontend/images/SO_logo.jpg')}}">
    <meta property="og:site_name" content="Stílus és Otthon ">
    <meta property="og:description" content="@yield('meta-description')">


    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    @yield('meta')
    @section('style')
        <link rel="stylesheet" href="{{ URL::asset('assetts/frontend/app.css') }}">
        @show
                <!-- App -->
        <script>
            window.App = window.App || {};
            App.siteURL = '{{ URL::to("/") }}';
            App.currentURL = '{{ URL::current() }}';
            App.fullURL = '{{ URL::full() }}';
            App.apiURL = '{{ URL::to("api") }}';
            App.assetURL = '{{ URL::to("assets") }}';
        </script>

        <!-- jQuery and Modernizr -->
        <script src="{{ URL::asset('assets/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="{{ URL::asset("assets/js/vendor/jquery-1.10.1.min.js") }}"><\/script>')</script>
        @yield('script.header')

</head>
<body class="{{Request::segment(1) ? Request::segment(1) : 'home'}} {{Auth::user() ? 'logged-in' : ' '}} ">
@include('frontend._partials.nav')

@if (count($errors) > 0)
    <div class="container">
        <div class="row">

    <div class="col-xs-12">
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    </div>

        </div>
    </div>
    @endif

    @yield('content')
    @section('script.footer')
            <!-- Script Footer -->
    <script src="{{ URL::asset('assetts/frontend/js/app.js') }}"></script>



    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-69545067-1', 'auto');
        ga('send', 'pageview');

    </script>


    @show

</body>
</html>
