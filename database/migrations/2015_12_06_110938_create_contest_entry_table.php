<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContestEntryTable extends Migration {

	public function up()
	{
		Schema::create('contest_entry', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('contest_user_id')->unsigned();
			$table->string('style', 30);
			$table->string('eredeti_alaprajz', 50);
			$table->string('bontasi_rajz', 50);
			$table->string('berendezesi_alaprajz', 50);
			$table->string('falnezet_konyha', 50);
			$table->string('falburkolasi_terv', 50);
			$table->string('padloburkolat_kiosztas', 50);
			$table->string('kapcsolasi_rajz', 50);
			$table->string('muleiras', 50);
			$table->string('konszignacios_tabla', 50);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contest_entry');
	}
}