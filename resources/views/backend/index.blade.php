@extends('backend.dashboard')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h1>Kedves {{\Auth::user()->name}}!</h1>
        </div>
        <div class="col-xs-12">
            <div class="alert alert-danger text-center">
                <h2> Kedves Tervezők!</h2>


                <p>Felhívjuk figyelmeteket, hogy a pályamunkák leadásának határidejét meghosszabbítottuk. Új leadási
                    határidő: 2016. február 7. 24:00 óra!
                    A "Feltöltés" menüpont aktív! Nagyon fontos, hogy a határidő-hosszabbítás ellenére mindenki végezze
                    el a következő lépéseket!
                </p>

                <p> Kérünk, hogy a következő lépéseket akkor is végezd el, hogyha még nem vagy teljesen kész a
                    pályamunkáddal!
                </p>
                <ol>
                <li>Kezdd meg személyes adatlapod kitöltésével a saját feltöltési felületed kialakítását.</li>
                <li>Töltsd fel a személyes adataidat, bemutatkozódat, elérhetőségeidet, valamint minimum egy darab
                    pályamunka képet.
                </li>
                <li>Mindezek feltöltésével teszteld a felületet 24 órán belül, győződj meg arról, hogy számodra minden
                    egyértelmű és érthető a rendszerben.
                </li>
                </ol>
               <p> Amennyiben a tesztelés közben kérdésetek merülne fel keressétek Török Bernadett munkatársunkat aki
                készséggel áll rendelkezésetekre.</p>

              <b>További jó munkát és sok sikert kívánunk!</b>
            </div>
            <div class="alert alert-warning">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <strong>Figyelem!</strong> A weboldal megtekintéséhez Google Chrome böngészőt, a PDF fájlok
                megnyitásához pedig az Adobe Reader programot javasoljuk.
            </div>
        </div>

        <div class="col-xs-12 col-sm-6 ">


            <div class="box box-solid box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">Tájékoztató</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">


                    <p> Köszöntjük a Stílus&Otthon Összefogás által kiírt Style&Draw pályázat oldalán.</p>

                    <p>
                        Ezen az oldalon találja meg a pályázati kiírás szövegét, amelyet kérjük, töltsön le és alaposan
                        olvasson végig! A pályázati kiírás figyelmes elolvasását követően kérjük, <a target="_blank"
                                                                                                     href="{{asset('files/palyazati_anyagok.zip')}}">töltse
                            le a két alaprajzot</a> *.pdf és/vagy *.dwg formátumban, majd válasszon közülük egyet,
                        amelyet
                        kidolgoz.
                    </p>

                    <p>

                        A következő dokumentum, melyre szüksége lesz a <a target="_blank"
                                                                          href="{{asset('files/nyilatkozat.pdf')}}">Nyilatkozat</a>,
                        kérjük, hogy ezt a fájlt töltse le,
                        nyomtassa ki, majd aláírást követően töltse vissza a megadott helyen az oldalra.
                    </p>

                    <p>
                        Kezdődhet a munka! A pályamunka során felhasználandó/felhasználható termékek köre és az ebben
                        támogatást nyújtó anyagok a
                        <a href="{{URL::to('verseny/panel/tervezesi-segedletek')}}">tervezési segédlet menüpont</a>
                        alatt, a
                        cégek/márkák logóira kattintva érhetőek el.
                        <br/> A tervezési segédletek folyamatosan bővülnek, így javasoljuk, hogy időnként látogasson
                        vissza
                        az oldalra, illetve figyelje tájékoztató jellegű leveleinket, melyekben többek között minden
                        feltöltött újdonságról is tájékoztatjuk.
                    </p>


                    <p> Kérdés estén tekintse, meg a Kérdése van? menüpontot, amennyiben ott nem talál választ a
                        kérdésére,
                        kérjük, vegye fel Velünk a kapcsolatot a
                        <a href="mailto:lakberendezo@styleandhome.hu">lakberendezo@styleandhome.hu</a> e-mail címen.
                    </p>

                    <p>
                        <b>
                            Ne felejtse, a pályaművek leadásának határideje: 2016. január 31. 24:00 óra
                        </b>
                    </p>

                    <p> Jó munkát és nagyon sok sikert kívánunk a pályázathoz!


                    </p>
                </div>
                <!-- /.box-body -->


            </div>
            <!-- /.box -->
        </div>
        <div class=" col-xs-12 col-sm-6">
            <div>
                <div class="box box-solid  box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title">Dokumentumok</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        <ul>
                            <li><a target="_blank" href="{{asset('files/palyazati_kiiras.pdf')}}">Pályázati kiírás</a>
                            </li>
                            <li><a target="_blank" href="{{asset('files/palyazati_anyagok.zip')}}">Alaprajzok</a></li>
                            <li><a target="_blank" href="{{asset('files/nyilatkozat.pdf')}}">Nyilatkozat</a></li>
                            <li><a target="_blank" href="{{asset('files/aszf.pdf')}}">ÁSZF</a></li>
                            <li><a target="_blank" href="{{asset('files/118539.pdf')}}">NAIH határozat</a></li>

                        </ul>
                    </div>
                </div>
            </div>


            <div>
                <div class="box box-solid {!! $profile_completion['status'] ? "box-success" :"box-default" !!}">
                    <div class="box-header with-border">
                        <h3 class="box-title">Profiladatok ellenőrző lista</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">

                        {!! $profile_completion['html'] !!}


                    </div>
                </div>

            </div>

        </div>


    </div>

    <div class="row">
        @foreach($completion as $complete)


            <div class="col-xs-12 col-sm-4">

                <div class="box box-solid   {!! $complete['status'] ? "box-success" :"box-default" !!}">
                    <div class="box-header with-border">
                        <h3 class="box-title">{!!  $complete['name'] !!} ellenőrző lista</h3>
                    </div>
                    <!-- /.box-header -->

                    <div class="box-body">
                        {!! $complete['html'] !!}
                    </div>
                    <div style="margin-bottom:0"
                         class="box-footer alert {!! ($complete['count'] > 30) ? 'alert-warning' : 'alert-success'  !!} text-center">


                        Maximális megengedett kép az összes helyiséget tekintve <span
                                class="text-bold">{!! $complete['count'] .'/30' !!}</span>

                    </div>
                </div>
            </div>


        @endforeach
    </div>

@endsection