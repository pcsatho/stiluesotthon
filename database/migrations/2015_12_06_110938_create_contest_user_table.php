<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateContestUserTable extends Migration {

	public function up()
	{
		Schema::create('contest_user', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('user_id')->unsigned();
			$table->string('info', 150);
			$table->string('website', 50)->nullable();
			$table->string('phone', 15);
			$table->string('school', 50);
			$table->string('profile_pic', 50);
			$table->string('name', 50);
			$table->string('school_document', 50);
			$table->string('agreement', 50);
			$table->string('postal_address', 70);
			$table->boolean('finished');
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('contest_user');
	}
}