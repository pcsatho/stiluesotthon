@extends('frontend.layouts.default')
@section('title')
    Köszönjük a regisztrációt! - Stilus és Otthon
@stop
@section('meta')
    <meta http-equiv="refresh" content="3;url={{URL::to('verseny/panel')}}">
@stop()
@section('content')
    <div class="container">
        <div class="row text-center">
        <h1 >Köszönjük a regisztrációt!</h1>
            <p >Hamarosan továbirányítjuk az adminisztrációs felületre...</p>
        </div>
    </div>


@stop()