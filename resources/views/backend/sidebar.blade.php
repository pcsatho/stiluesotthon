<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            {{--      <div class="pull-left image">
                    <img src="{{ asset("/bower_components/admin-lte/dist/img/user2-160x160.jpg") }}" class="img-circle" alt="User Image" />
                  </div>--}}
            <div class="info">
                <p>{{\Auth::user()->name}}</p>

                <p class="text-muted">({{\Auth::user()->unique}})</p>
            </div>
        </div>


        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">Menü</li>
            <!-- Optionally, you can add icons to the links -->
            <li class="{{ Request::is('verseny/panel') ? 'active' : '' }}"><a href="{{URL::to('verseny/panel')}}"><span>Kezdőlap</span></a>
            </li>
            <li class="{{ Request::is('verseny/panel/Profil') ? 'active' : '' }}"><a
                        href="{{URL::to('verseny/panel/profil')}}">Profil-adatok</a></li>
            <li class="{{ Request::is('verseny/panel/tervezesi-segedletek') ? 'active' : '' }}"><a
                        href="{{URL::to('verseny/panel/tervezesi-segedletek')}}"><span>Tervezési segédletek</span></a>
            </li>

            <li class="{{ Request::is('verseny/panel/palyazati-anyagok') ? 'active' : '' }}"><a
                        href="{{URL::to('verseny/panel/palyazati-anyagok')}}">Feltöltés</a></li>

            {{--      <li><a href="#"><span>Another Link</span></a></li>
                  <li class="treeview">
                    <a href="#"><span>Multilevel</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                      <li><a href="#">Link in level 2</a></li>
                      <li><a href="#">Link in level 2</a></li>
                    </ul>
                  </li>--}}
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>