<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddEpitesiRajzToContentEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contest_entry', function (Blueprint $table) {
            $table->string( 'epitesi_rajz', 50 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest_entry', function (Blueprint $table)
        {
            $table->dropColumn(array('epitesi_rajz'));
        });
    }
}
