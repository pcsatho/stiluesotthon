<!-- Main Header -->
<header class="main-header">

  <!-- Logo -->
  <a href="{{URL::to('/')}}" class="logo"><b>Style</b>&<b>Draw</b></a>

  <!-- Header Navbar -->
  <nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Navigáció</span>
    </a>
    <!-- Navbar Right Menu -->
    <div class="navbar-custom-menu">
      <ul class="nav navbar-nav">

<li>
  <a href="{{URL::to('kijelentkezes')}}">Kijelentkezés</a>
</li>
      </ul>
    </div>
  </nav>
</header>