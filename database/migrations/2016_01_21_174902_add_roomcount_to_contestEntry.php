<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRoomcountToContestEntry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contest_image', function (Blueprint $table) {
            $table->enum('roomcount', ['1', '2','3']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contest_image', function (Blueprint $table) {
            $table->dropColumn('roomcount');
        });
    }
}
