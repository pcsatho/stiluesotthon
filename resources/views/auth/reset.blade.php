@extends('frontend.layouts.default')
@section('title')
    Jelszó megváltoztatása - Stilus és Otthon
@stop
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xs-8 col-xs-push-2 col-md-6 col-md-push-3">

                <form method="POST" action="{{URL::to('/elfelejtett-jelszo/reset')}}">
                    {!! csrf_field() !!}

                    <input type="hidden" name="token" value="{{ $token }}">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach

                            </ul>
                        </div>
                    @endif


                    <div class="form-group">
                        <label for="email">E-mail cím</label>
                        <input type="email" name="email" class="form-control" value="{{ old('email') }}">
                    </div>

                    <div class="form-group">
                        <label for="password">Jelszó</label>
                        <input type="password" name="password" class="form-control">
                    </div>

                         <div class="form-group">
                        <label for="password_confirmation">Jelszó megerősítése</label>
                        <input type="password" name="password_confirmation" class="form-control">
                    </div>

                    <div class="form-group">
                        <button class="btn btn-primary btn-block" type="submit">Jelszó megváltoztatása</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop
