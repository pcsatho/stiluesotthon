<?php

namespace App\Http\Controllers\Contest;

use App\contestEntry;
use App\contestImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\contestEntryRequest;

class EntryController extends Controller {




	public function getUploadIndex()
	{
		$entries = \Auth::user()->contestant->entries;

		return view('backend.upload.index')->withEntries($entries);
	}


	public function getUpload()
	{
		return view('backend.upload.upload');
	}


	public function postUpload(contestEntryRequest $request)
	{
		$entry = new contestEntry();


		if($this->storeFields($entry,$request)){
			return redirect()->route('getUploadIndex')->withMessage('Pályamunka elmentve!');
		}

	}

	public function getEditUpload($id)
	{
		$entry = contestEntry::find($id);



		return view('backend.upload.upload')->withEntry($entry);
	}


	public function postEditUpload($id, contestEntryRequest $request)
	{


		$entry = contestEntry::find($id);

		if ( $this->storeFields($entry, $request,$id) )
		{
			return redirect()->back()->withMessage('Pályamunka elmentve!');
		}
	}


	public function getDeleteUpload($id)
	{
		$entry = contestEntry::find($id);
		if($entry->delete()){
		return \Redirect::back()->withMessage('A törlés sikeres!');
		}

		return \Redirect::back()->withMessage('A törlés sikertelen!');
	}


	private function storeFields($entry,$request,$id=null){

		$entry->name            = $request->name;
		$entry->info            = $request->info;
		$entry->style           = $request->style;
		$entry->entry_category          = $request->entry_category;
		$entry->contest_user_id = \Auth::user()->contestant->id;

		foreach(contestEntry::$fileFields as $field=>$fieldName){
			if ( $name = $this->moveUploadedFile($request, $field,$id) )
			{
				$entry->$field = $name;

			}
		}

		if($request->has('contestImage')){

			foreach ($request->get('contestImage') as $image_id => $atts){

				$image = contestImage::find($image_id);
				$image->name = !empty($atts['name']) && isset($atts['name'])  ? $atts['name'] : '';
				$image->roomcount = !empty($atts['roomcount']) && isset($atts['roomcount'])  ? $atts['roomcount'] : '';
				$image->save();

			}
		}



		return $entry->save();
	}


	private function moveUploadedFile(contestEntryRequest $request, $field,$id)
	{

		if ( $request->hasFile($field) )
		{
			if($id){
						$userDir   = public_path() . '/entries/' . \Auth::user()->unique . '/'.$id.'/';
			}else{
						$userDir = public_path() . '/entries/' . \Auth::user()->unique . '/';
			}
			if ( ! \File::isDirectory($userDir) )
			{
				\File::makeDirectory($userDir,0775,true);
			}

			$fileName = \Auth::user()->unique . '-' . $field . '.' . $request->file($field)->guessClientExtension();

			if ( $request->file($field)->move($userDir, $fileName) )
			{
				return \Auth::user()->unique . '-' . $field . '.' . $request->file($field)->guessClientExtension();
			}
		}

		return false;

	}


	public function finalizeEntries()
	{
		$user = \Auth::user()->contestant;
		$user->finished = 1;
		$user->save();
		return redirect()->back()->withMessage('Pályamunkák elküldve! Köszönjük, hogy részt veszel pályázatunkon! Sok sikert!');

	}


}
