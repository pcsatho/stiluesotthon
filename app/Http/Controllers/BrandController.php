<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class BrandController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        /** Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\Category);
			$this->filter->add('name', 'Name', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('name', 'Name');
			$this->grid->add('code', 'Code');
			$this->addStylesToGrid();

        */
	    $this->grid = \DataGrid::source(\DataFilter::source(new \App\Brand));
	    $this->grid->add('name', 'Márka neve');

	    $this->addStylesToGrid();
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

        /* Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\Category());

			$this->edit->label('Edit Category');

			$this->edit->add('name', 'Name', 'text');
		
			$this->edit->add('code', 'Code', 'text')->rule('required');


        */
	    $this->edit = \DataEdit::source(new \App\Brand());

	    $this->edit->label('Márka feltöltése/szerkesztése');

	    $this->edit->add('name', 'Márka neve', 'text')->rule('required');

	    $this->edit->add('logo', 'Márka logo', 'image')->rule('required')->move('images/brand')->preview(80,80);
	    $this->edit->add('partner_id','Mely partnerhez tartozik?','select')->options(\App\Partner::lists("name", "id")->all());
        return $this->returnEditView();
    }    
}
