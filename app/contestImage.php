<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contestImage extends Model
{

	protected $table = 'contest_image';
	protected $guarded = [ 'id' ];

	public function contest_entry(){
		return $this->belongsTo('App\contestEntry');
   }


	public static function checkByRoom($room, $entry)
	{
		$response = array();
		$image = contestImage::where('contest_entry_id','=', $entry->id)->where('category','=',$room)->get();

		$count = $image->count();
		$response['count'] = $count;

			if($room == 'moodboard'){
				$response['max'] = 10;
				$response['status'] = ($count >= 5) ? true : false;
			}else{
				$response['max']    = 9;
				$response['status'] = ( $count >= 1 ) ? true : false;
			}

		return $response;
	}


}
