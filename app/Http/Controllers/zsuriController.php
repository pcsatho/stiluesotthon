<?php 

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use \Serverfireteam\Panel\CrudController;

use Illuminate\Http\Request;

class zsuriController extends CrudController{

    public function all($entity){
        parent::all($entity); 

        /** Simple code of  filter and grid part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields


			$this->filter = \DataFilter::source(new \App\Category);
			$this->filter->add('name', 'Name', 'text');
			$this->filter->submit('search');
			$this->filter->reset('reset');
			$this->filter->build();

			$this->grid = \DataGrid::source($this->filter);
			$this->grid->add('name', 'Name');
			$this->grid->add('code', 'Code');
			$this->addStylesToGrid();

        */
		$this->filter = \DataFilter::source(new \App\zsuri);
		$this->filter->add('name', 'Név', 'text');

		$this->filter->submit('search');
		$this->filter->reset('reset');
		$this->filter->build();

		$this->grid = \DataGrid::source($this->filter);

		$this->grid->add('name', 'Név');
	    $this->grid->add('position', 'Pozíció');

		$this->addStylesToGrid('position',20);
        return $this->returnView();
    }
    
    public function  edit($entity){
        
        parent::edit($entity);

        /* Simple code of  edit part , List of all fields here : http://laravelpanel.com/docs/master/crud-fields
	
			$this->edit = \DataEdit::source(new \App\Category());

			$this->edit->label('Edit Category');

			$this->edit->add('name', 'Name', 'text');
		
			$this->edit->add('code', 'Code', 'text')->rule('required');


        */
		$this->edit = \DataEdit::source(new \App\zsuri());

		$this->edit->label('Zsűritag szerkesztése');

		$this->edit->add('name', 'Név', 'text')->rule('required');
		$this->edit->add('title', 'Titulus', 'text')->rule('required');
		$this->edit->add('about', 'Bemutatkozás', 'redactor')->rule('required');
		$this->edit->add('url', 'Weboldal', 'text')->rule('required');
		$this->edit->add('goals', 'Elért sikerek', 'redactor');
		$this->edit->add('image', 'Arckép', 'image')->move('images/zsuri')->preview(80, 80);
		$this->edit->add('position', 'Pozíció', 'text')->rule('required');


		return $this->returnEditView();
    }    
}
