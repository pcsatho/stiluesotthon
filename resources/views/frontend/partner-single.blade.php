@extends('frontend.layouts.default')
@section('title')
    {{$partner->name}} - Stilus és Otthon
@stop
@section('content')
<div id="partner-single" class="container">
    <div class="row">
        <div class="header col-xs-12">
            <div data-mh="partner" class="col-xs-12 col-sm-2 ">
                <img src="{{'/images/partner/'.$partner->logo}}" alt="{{$partner->name}}"
                     class="img-responsive center-block"/>
            </div>
            <div class="details col-xs-12 col-sm-8" data-mh="partner">
                <div class="name"><h1>{{$partner->name}}</h1></div>
<div class="contact">

                @if(!empty($partner->phone))
                    <div class="phone"><span>Tel.:</span> <a data-trigger="hover focus" data-toggle="tooltip"
                                                              data-placement="bottom"
                                                              title="Hívjon minket a {{$partner->phone}} telefonszámon" href="tel:{{$partner->phone}}">{{$partner->phone}}</a></div> @endif
                @if(!empty($partner->mail))
                    <div class="mail"><span>E-mail:</span> <a data-trigger="hover focus" data-toggle="tooltip"
                                                              data-placement="bottom"
                                                              title="Írjon nekünk a {{$partner->mail}} e-mail címre!" href="mailto:{{$partner->mail}}">{{$partner->mail}}</a></div> @endif
                @if(!empty($partner->web))
                    <div class="web"><span>Weboldal:</span> <a href="{{$partner->web}}" target="_blank">{{$partner_link}}</a>@if(!empty($partner->second_url)) &nbsp; &nbsp;
                        <a href="{{$partner->second_url}}" target="_blank"> {{$partner_second_link}}</a> @endif


                    </div>

                        @if(!empty($partner->second_url))  @endif
                    @endif
</div>
            </div>
        </div>
        <div class="about col-xs-12 col-sm-8 col-sm-push-2">
            <div class="col-xs-12">
            {!! $partner->about !!}

                @if($partner->distributors)
                    <p>
                        <a href="{{$partner->distributors}}" target="_blank" name="{{$partner->name .'viszonteladói, partnerei'}}">Partnerek, viszonteladók listája</a>
                    </p>
                @endif
            </div>
        </div>


    </div>

    @if(count($partner->brands))
        <div class="row">

            <div id="brands" class="col-xs-12 col-sm-8 col-sm-push-2">
            <h2 class="col-xs-12">{{$partner->name}} által forgalmazott márkák</h2>
                @foreach($partner->brands as $brand)
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <img class="img-responsive center-block" src="{{'/images/brand/'.$brand->logo}}" alt="{{$brand->name}}"/>
                </div>
            @endforeach
        </div>
        </div>
    @endif
</div>
@stop()