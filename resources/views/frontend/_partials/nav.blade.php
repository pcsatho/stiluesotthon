@if(Auth::check())
    <nav id="logged-in-bar" class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <ul class="nav navbar-nav navbar-right">

                    <li><a href="{{URL::to('verseny/panel')}}">Adminisztrációs felület</a></li>

                <li><a href="{{URL::to('/kijelentkezes')}}">Kijelentkezés</a></li>

            </ul>
        </div>
    </nav>

@endif
<nav id="top-bar" class="navbar navbar-default">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="col-xs-12 col-sm-4 col-md-2">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                    data-target="#main-menu" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{URL::to('/')}}"> <i class="so-icon so-icon-logo so-icon-2x red"></i></a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class=" col-xs-12 col-sm-8 col-md-8 ">
            <div id="main-menu" class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="{{ Request::is('partnerek*') ? 'active' : '' }}"><a href="{{URL::to('partnerek')}}">Partnereink</a>
                    </li>
                    <li class="{{ Request::is('zsuri*') ? 'active' : '' }}"><a href="{{URL::to('zsuri')}}">Zsűri</a></li>
                    <li class="{{ Request::is('faq') ? 'active' : '' }}"><a href="{{URL::to('faq')}}">Kérdése van?</a>
                    </li>
                    <li class="{{ Request::is('kapcsolat') ? 'active' : '' }}"><a href="{{URL::to('kapcsolat')}}">Kapcsolat</a>
                    </li>                    <li class="{{ Request::is('verseny/bejelentkezes') ? 'active' : '' }}">
                        <a href="{{URL::to('verseny/bejelentkezes')}}">Bejelentkezés</a>
                    </li>

                </ul>
            </div>
        </div>
        <!-- /.navbar-collapse -->

        <div class="col-xs-12 col-sm-12 hidden-xs col-md-2 social">
            <ul class="nav navbar-nav">
                <li>
                    <a target="_blank" data-trigger="hover focus" data-toggle="tooltip" data-placement="bottom" title="Keressen minket facebookon!" href="https://www.facebook.com/stilusesotthon/">
                        <i class="so-icon so-icon-f_icon "></i>
                    </a>
                </li>
                <li>
                    <a data-trigger="hover focus" data-toggle="tooltip" data-placement="bottom" title="Írjon nekünk a lakberendezo@styleandhome.hu e-mail címre" href="mailto:lakberendezo@styleandhome.hu">
                        <i class="so-icon so-icon-letter_icon"></i>
                    </a>
                </li>
            </ul>
        </div>

    </div>

</nav>